import { UserPanelComponent } from './components/panels/user-panel/user-panel.component';
import { AdminPanelComponent } from './components/panels/admin-panel/admin-panel.component';
import { ProductsComponent } from './components/products/products.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './routing/app.routing';
import { AppComponent } from './app.component';
import { AuthComponent } from './components/auth/auth.component';
import { CategoriesComponent } from './components/category/categories.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { JwtInterseptor } from './helpers/jwt-interseptor';
import { ErrorComponent } from './components/error/error.component';
import { PanelComponent } from './components/panels/panel/panel.component';
import { EnumToKeyValuePipe } from './pipes/enumToKeyValue';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ProductComponent } from './components/product/product.component';
import { ModeratorPanelComponent } from './components/panels/moderator-panel/moderator-panel.component';

@NgModule({
  declarations: [
    AppComponent,
    AuthComponent,
    CategoriesComponent,
    ProductsComponent,
    ProductComponent,
    PanelComponent,
    AdminPanelComponent,
    ModeratorPanelComponent,
    UserPanelComponent,
    ErrorComponent,
    EnumToKeyValuePipe
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FontAwesomeModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS, useClass: JwtInterseptor, multi: true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
