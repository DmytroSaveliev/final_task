import { AuthService } from '../../services/auth.service';
import { FormControl, FormGroup } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { CurrentUser } from '../../interfaces/user-current';

@Component({
  selector: 'app-auth',
  templateUrl: 'auth.component.html',
  styleUrls: ['auth.component.scss']
})

export class AuthComponent implements OnInit {

  registerForm!: FormGroup;
  registerErrors!: string[];
  registerSuccess!: string;
  loginForm!: FormGroup;
  loginErrors!: string[];

  currentUser!: CurrentUser | null;

  constructor(private authService: AuthService) { }

  ngOnInit(): void {

    this.currentUser = this.authService.getCurrentUser();

    this.registerErrors = [];
    this.registerForm = new FormGroup({
      email: new FormControl(''),
      password: new FormControl('')
    });

    this.loginErrors = [];
    this.loginForm = new FormGroup({
      email: new FormControl(''),
      password: new FormControl('')
    });
  }

  isAuthenticated(): boolean {
    return this.currentUser !== null;
  }

  login(): void {
    this.authService.login(this.loginForm.value).subscribe(
      (user: any) => {
        this.currentUser = user;
        this.loginErrors = [];
      },
      (exc: any) => {
        this.loginErrors = exc.error.errors ? exc.error.errors : ['unknown error'];
      }
    );
  }

  register(): void {
    this.authService.registerUser(this.registerForm.value).subscribe(
      (response: any) => {
        this.registerSuccess = 'You have successfully registered';
        this.registerErrors = [];
        this.registerForm.reset();
      },
      (exc: any) => {
        this.registerSuccess = '';
        this.registerErrors = exc.error.errors ? exc.error.errors : ['unknown error'];
      }
    );
  }

  logout(): void {
    this.authService.logout();
    this.currentUser = null;
  }
}
