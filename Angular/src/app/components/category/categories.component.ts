import { CategoryService } from './../../services/category.service';
import { Component, OnInit } from '@angular/core';
import { Category } from 'src/app/interfaces/category';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit {

  categories!: Category[];

  constructor(private categoryService: CategoryService) { }

  ngOnInit(): void {

      this.categoryService.getCategories().subscribe(
      (test: Category[]) => {
        this.categories = test;
      },
      (exc: any) => {
        console.dir(exc);
      }
    );
  }
}
