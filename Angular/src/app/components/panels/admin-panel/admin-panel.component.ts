import { AuthService } from './../../../services/auth.service';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-admin-panel',
  templateUrl: './admin-panel.component.html',
  styleUrls: ['./admin-panel.component.scss']
})
export class AdminPanelComponent implements OnInit {

  moderatorForm!: FormGroup;
  moderatorErrors!: string[];
  moderatorSuccess!: string;

  constructor(private authService: AuthService) { }

  ngOnInit(): void {

    this.moderatorErrors = [];
    this.moderatorForm = new FormGroup({
      email: new FormControl(''),
      password: new FormControl('')
    });
  }

  addModerator(): void {

    this.moderatorForm.disable();

    this.authService.registerModerator(this.moderatorForm.value).subscribe(
      (response: any) => {
        this.moderatorSuccess = 'Moderator has been successfully registered';
        this.moderatorErrors = [];
        this.moderatorForm.reset();
        this.moderatorForm.enable();
      },
      (exc: any) => {
        this.moderatorSuccess = '';
        this.moderatorErrors = exc.error?.errors ? exc.error.errors : ['unknown error'];
        this.moderatorForm.enable();
      }
    );
  }
}
