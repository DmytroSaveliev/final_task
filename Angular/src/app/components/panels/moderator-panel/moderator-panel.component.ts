import { AuthService } from './../../../services/auth.service';
import { Component, OnInit } from '@angular/core';
import { Category } from 'src/app/interfaces/category';
import { Product } from 'src/app/interfaces/product';
import { ModerationService } from 'src/app/services/moderation.service';
import { Status } from 'src/app/interfaces/status';
import { ProductStatus } from 'src/app/interfaces/product-status';

@Component({
  selector: 'app-moderator-panel',
  templateUrl: './moderator-panel.component.html',
  styleUrls: ['./moderator-panel.component.scss']
})
export class ModeratorPanelComponent implements OnInit {

  categories!: Category[];
  categoriesLoaded!: boolean;

  products!: Product[];
  productsLoaded!: boolean;

  statuses = Status;

  ProductStatus = ProductStatus;

  globalErrors!: string[];

  constructor(private authService: AuthService, private moderationService: ModerationService) { }

  ngOnInit(): void {

    this.categoriesLoaded = false;
    this.productsLoaded = false;

    this.loadCategories(false);
  }

   loadCategories(force: boolean): void {

    if (!force && this.categoriesLoaded) {
      return;
    }

    this.moderationService.getCategories(null).subscribe(
      (response: Category[]) => {
        this.categories = response;
        this.categoriesLoaded = true;
      },
      (exc: any) => {
      }
    );
  }

  loadNewProducts(force: boolean): void {

    if (!force && this.productsLoaded) {
      return;
    }

    this.moderationService.getProducts(10, 1, ProductStatus.New).subscribe(
      (response: Product[]) => {
        this.products = response;
        this.productsLoaded = true;
      },
      (exc: any) => {
      }
    );
  }

  modifyProduct(id: number, status: ProductStatus, lastModified: string): void {

    this.globalErrors = [];

    this.moderationService.modifyProduct(id, { status, lastModified }).subscribe(
      (category: Category) => {
        this.loadNewProducts(true);
      },
      (exc: any) => {
        this.globalErrors = exc.error?.errors ? exc.error.errors : ['Unknown error'];
      }
    );
  }

  modifyCategory(id: number, status: Status, lastModified: string): void {

    this.globalErrors = [];

    this.moderationService.modifyCategory(id, { status, lastModified }).subscribe(
      (category: Category) => {
        this.loadCategories(true);
      },
      (exc: any) => {
        this.globalErrors = exc.error?.errors ? exc.error.errors : ['Unknown error'];
      }
    );
  }

  modifySubCategory(categoryId: number, id: number, status: Status, lastModified: string): void {

    this.globalErrors = [];

    this.moderationService.modifySubCategory(categoryId, id, { status, lastModified }).subscribe(
      (category: Category) => {
        this.loadCategories(true);
      },
      (exc: any) => {
        this.globalErrors = exc.error?.errors ? exc.error.errors : ['Unknown error'];
      }
    );
  }
}
