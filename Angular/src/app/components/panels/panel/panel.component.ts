import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-panel',
  templateUrl: './panel.component.html',
  styleUrls: ['./panel.component.scss']
})
export class PanelComponent implements OnInit {

  roles!: string[];

  constructor(private authService: AuthService) { }

  ngOnInit(): void {

    this.roles = this.authService.getCurrentUser()?.roles ?? [];
  }

  isInRole(role: string): boolean {

    return this.roles.includes(role);
  }

  testPanel(): string {

    return 'method from panel';
  }
}