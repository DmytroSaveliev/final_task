import { AuctionConfiguration } from './../../../interfaces/auction-configuration';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { AuctionConfigurationService } from 'src/app/services/auction-configuration.service';
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'app-user-panel',
  templateUrl: './user-panel.component.html',
  styleUrls: ['./user-panel.component.scss']
})
export class UserPanelComponent implements OnInit {

  auctionConfiguration!: AuctionConfiguration;

  newProductForm!: FormGroup;
  newProductErrors!: string[];
  newProductSuccess!: string;

  globalErrors!: string[];

  constructor(
    private authService: AuthService,
    private auctionConfigurationService: AuctionConfigurationService,
    private productService: ProductService) { }

  ngOnInit(): void {

    this.globalErrors = [];

    this.auctionConfigurationService.getAuctionConfiguration().subscribe(
      (ac: any) => {
        this.auctionConfiguration = ac;
      },
      (exc: any) => {
        this.globalErrors = exc.error.errors ? exc.error.errors : ['Unknown error'];
      }
    );

    this.newProductErrors = [];
    this.newProductForm = new FormGroup({
      title: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required),
      subCategoryName: new FormControl('', Validators.required),
      details: new FormGroup({
        quality: new FormControl(0, Validators.required),
        year: new FormControl(0, Validators.required),
      }),
      betConfiguration: new FormGroup({
        buyItNowPrice: new FormControl({value: null, disabled: true}, Validators.required),
        minPrice: new FormControl({value: null, disabled: true}, Validators.required),
        startPrice: new FormControl(0, Validators.required),
        auctionPeriod: new FormControl(0, Validators.required),
        minBetCapacity: new FormControl(0, Validators.required),
        maxBetCapacity: new FormControl({value: null, disabled: true}, Validators.required),
      })
    });
  }

  toggleNullable(input: any, seed: any, name: string): void {

    const target = this.newProductForm.get(name);
    if (!target) { return; }

    if (input.checked === true) {
      target.enable();
      target.setValue(seed);
    }
    else {
      target.disable();
      target.setValue(null);
    }
  }
 
  addProduct(): void {

    this.productService.addProduct(this.newProductForm.value).subscribe(
      (response: any) => {
        this.newProductSuccess = 'Product has been successfully added';
        this.newProductErrors = [];
        this.newProductForm.reset();
      },
      (exc: any) => {
        this.newProductSuccess = '';
        this.newProductErrors = exc.error?.errors ? exc.error.errors : ['unknown error'];
      }
    );
  }
}
