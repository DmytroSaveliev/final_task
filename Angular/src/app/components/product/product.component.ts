import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Bet } from 'src/app/interfaces/bet';
import { Product } from 'src/app/interfaces/product';
import { ProductStatus } from 'src/app/interfaces/product-status';
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {

  value = 0;
  productStatus = ProductStatus;
  product!: Product;
  bets: Bet[];
  betErrors!: string[];
  needReloadStatuses = [
    'Product has been modified before',
    'Auction expired',
    'Auction completed'
  ];

  constructor(
    private productService: ProductService,
    private activatedRoute: ActivatedRoute,
    private router: Router) {

      this.bets = Array();
    }

  ngOnInit(): void {

    this.activatedRoute.queryParams.subscribe(params => {

      const id = params.id;

      if (!id) {
        this.router.navigate(['404'], { skipLocationChange: true });
        return;
      }

      this.loadProduct(id);
      this.loadBets(id);
    });
  }

  getCurrentBetValue(): number {

    if (this.bets && this.bets[0]) {
      return this.bets[0].value;
    }
    else {
      return this.product.betConfiguration?.startPrice || 0;
    }
  }

  getMinBetValue(): number | null {

    if (!this.product || !this.product.betConfiguration) {
      return null;
    }

    const maxValue = this.product.betConfiguration.maxValue;
    const minBetValue = this.getCurrentBetValue() + this.product.betConfiguration.minBetCapacity;

    if (minBetValue > maxValue) {
      return maxValue;
    }

    return minBetValue;
  }

  getMaxBetValue(): number | null {

    if (!this.product || !this.product.betConfiguration) {
      return null;
    }

    const maxBetCapacity = this.product.betConfiguration.maxBetCapacity;
    const maxValue = this.product.betConfiguration.maxValue;

    if (maxBetCapacity == null) {
      return maxValue;
    }

    const maxBetValue = this.getCurrentBetValue() + maxBetCapacity;

    return maxBetValue > maxValue ? maxValue : maxBetValue;
  }

  isValidBetValue(): boolean {


    const minBetValue = this.getMinBetValue();
    const maxBetValue = this.getMaxBetValue();



    if (!minBetValue || !maxBetValue) {
      return false;
    }

    return this.value >= minBetValue && this.value <= maxBetValue;
  }

  loadProduct(id: number): void {
    this.productService.getProduct(id).subscribe(
      (product: Product) => {
        this.product = product;
      },
      (exc: any) => {
        if (exc.status === 404) {
          this.router.navigate(['404'], { skipLocationChange: true });
        }
      }
    );
  }

  makeBet(value: number): void {

    this.betErrors = [];

    if (!this.product) {
      return;
    }

    const id = this.product.id;
    const lastMod = this.product.lastModified;

    this.productService.postBet(id, value, lastMod).subscribe(
      (response: any) => {
        this.loadProduct(id);
        this.loadBets(id);
      },
      (exc: any) => {
        this.betErrors = exc.error?.errors ? exc.error.errors : ['unknown error'];
        this.reloadIfNeeded(id, this.betErrors);
      }
    );
  }

  loadBets(productId: number): void {
    this.productService.getBets(productId).subscribe(
      (bets: Bet[]) => {
        this.bets = bets;
      },
      (exc: any) => {
      }
    );
  }

  reloadIfNeeded(id: number, errors: string[]): void {
    if (errors.some(e => this.needReloadStatuses.includes(e))) {
      this.loadProduct(id);
      this.loadBets(id);
    }
  }
}
