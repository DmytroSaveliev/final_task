import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Category } from 'src/app/interfaces/category';
import { Products } from 'src/app/interfaces/products';
import { SubCategory } from 'src/app/interfaces/subcategory';
import { CategoryService } from 'src/app/services/category.service';
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {

  public title!: string;
  limit = 10;
  page!: number;
  categoryId!: number | null;
  subCategoryId!: number | null;

  categories!: Category[];
  products!: Products;
  subCategory!: SubCategory;

  constructor(
    private categoryService: CategoryService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private productService: ProductService) { }

  ngOnInit(): void {

    this.loadCategories();

    this.activatedRoute.queryParams.subscribe(params => {

      this.page = params.page || 1;
      this.categoryId = params.categoryId || null;
      this.subCategoryId = params.subCategoryId || null;

      this.loadProducts(this.limit, this.page, this.subCategoryId);
      if (this.categoryId || this.subCategoryId) {
        this.loadSubCategory(this.categoryId, this.subCategoryId);
      }
      else {
        this.title = 'All products';
      }
    });
  }

  pageChanged(): void {
    const queryParams: any = {};
    if (this.categoryId) {
      queryParams.categoryId = this.categoryId;
    }
    if (this.subCategoryId) {
      queryParams.subCategoryId = this.subCategoryId;
    }
    queryParams.page = this.page;

    this.router.navigate([], {queryParams});
  }

  loadCategories(): void {
    this.categoryService.getCategories().subscribe(
      (categories: Category[]) => {
        this.categories = categories;
      },
      (exc: any) => {
        console.dir(exc);
      }
    );
  }

  loadProducts(
    limit: number,
    page: number,
    subCategoryId: number | null = null): void {
    this.productService.getProducts(limit, page, subCategoryId).subscribe(
      (response: any) => {
        this.products = response;
      },
      (exc: any) => {
        console.dir(exc);
      }
    );
  }

  loadSubCategory(categoryId: number | null, subCategoryId: number | null): void {

    if (categoryId === null || subCategoryId === null){

      this.router.navigate(['404'], { skipLocationChange: true });
      return;
    }

    this.categoryService.getSubCategory(categoryId, subCategoryId).subscribe(
      (subCategory: any) => {
        this.subCategory = subCategory;
        this.title = `${this.subCategory.category.name}: ${this.subCategory.name}`;
      },
      (exc: any) => {
        this.router.navigate(['404'], { skipLocationChange: true });
      }
    );
  }
}
