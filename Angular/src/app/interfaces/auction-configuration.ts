export interface AuctionConfiguration {

    maxAuctionPeriod: number;
    minAuctionPeriod: number;

    maxBetCapacity: number;
    minBetCapacity: number;

    betProlongationDuration: number;
    auctionProlongationPeriod: number;

    isActive: boolean | null;
}
