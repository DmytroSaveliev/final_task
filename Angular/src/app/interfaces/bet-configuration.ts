export interface BetConfiguration {

    buyItNowPrice: number | null;
    minPrice: number | null;
    startPrice: number;
    auctionPeriod: number;
    minBetCapacity: number;
    maxBetCapacity: number | null;
    maxValue: number;
}
