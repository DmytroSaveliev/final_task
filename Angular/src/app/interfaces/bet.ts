export interface Bet {

    userEmail: string;
    date: string;
    value: number;
}
