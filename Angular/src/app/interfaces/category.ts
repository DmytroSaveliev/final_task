import { Status } from './status';

export interface Category {

    id: number;
    name: string;
    subCategories: CategorySubCategory[];
    status: Status;
    lastModified: string;
}

export interface CategorySubCategory {

    id: number;
    name: string;
    status: Status;
    lastModified: string;
}