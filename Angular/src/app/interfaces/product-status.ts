
export enum ProductStatus {
    New = 0,
    Public,
    Competed,
    Expired,
    Deleted
}
