import { BetConfiguration } from './bet-configuration';
import { ProductStatus } from './product-status';

export interface Product {

    id: number;
    title: string;
    description: string;
    userEmail: string;
    quality: number;
    year: number;
    status: ProductStatus;
    lastModified: string;
    created: Date;
    expiration: Date;
    betConfiguration: BetConfiguration | null;
}
