import { Product } from './product';

export interface Products {

    count: number;
    products: Product[];
}
