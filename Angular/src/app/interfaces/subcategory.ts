import { Status } from './status';

export interface SubCategory {

    id: number;
    name: string;
    status: Status;
    lastModified: string;
    category: SubCategoryCategory;
}

export interface SubCategoryCategory {

    id: number;
    name: string;
    status: Status;
    lastModified: string;
}