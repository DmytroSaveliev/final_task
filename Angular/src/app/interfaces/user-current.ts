export interface CurrentUser {
    id: string;
    email: string;
    roles: string[];
    token: string;
}
