import { Pipe, PipeTransform } from '@angular/core';

/*
@Pipe({
  name: 'enumToArray'
})
export class EnumToArrayPipe implements PipeTransform {
  transform(data: object): string[] {
    const keys = Object.keys(data);
    return keys.slice(keys.length / 2);
  }
}
*/

interface KeyValue{
  key: number;
  value: string;
}


@Pipe({
  name: 'enumToKeyValue'
})
export class EnumToKeyValuePipe implements PipeTransform {
  transform(data: object): KeyValue[] {
    const keys = Object.keys(data);

    const keyValues: KeyValue[] = Array();
    let i = 0;

    keys.forEach((value, index, arr) => {

        if (!i && Number(value) < keys.length / 2) {
          keyValues.push({key: Number(value), value: ''});
        }
        else {
          keyValues[i++].value = value;
        }
    });

    return keyValues;
  }
}
