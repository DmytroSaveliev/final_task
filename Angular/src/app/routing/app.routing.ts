import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ErrorComponent } from '../components/error/error.component';
import { PanelComponent } from '../components/panels/panel/panel.component';
import { ProductComponent } from '../components/product/product.component';
import { ProductsComponent } from '../components/products/products.component';
import { AuthGuard } from '../helpers/auth-guard';

const routes: Routes = [
    { path: 'product', component: ProductComponent, },
    { path: 'products', component: ProductsComponent},
    { path: 'panel', component: PanelComponent, canActivate: [AuthGuard]},
    { path: '', component: ProductsComponent},
    { path: '404', component: ErrorComponent },
    { path: '**', component: ErrorComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
