import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuctionConfigurationService {

  constructor(private http: HttpClient) { }

  getAuctionConfiguration(): any {
    return this.http.get('https://localhost:44313/api/auctionconfiguration');
  }
}
