import { UserRequest } from './../interfaces/user-request';
import { CurrentUser } from '../interfaces/user-current';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private currentUserSubject: BehaviorSubject<CurrentUser | null>;
  public currentUser: Observable<CurrentUser | null>;

  constructor(private http: HttpClient) {

    this.currentUserSubject = new BehaviorSubject<CurrentUser | null>(this.userFromStorage());
    this.currentUser = this.currentUserSubject.asObservable();
  }

  private userFromStorage(): CurrentUser | null {

    const user = JSON.parse(localStorage.getItem('currentUser') || '{}');
    if (Object.keys(user).length === 0) {
      return null;
    }

    return user;
  }

  registerUser(userRegister: UserRequest): any {
    return this.http.post('https://localhost:44313/api/account/users', userRegister);
  }

  registerModerator(moderatorRegister: UserRequest): any {
    return this.http.post('https://localhost:44313/api/account/moderators', moderatorRegister);
  }

  login(userLogin: UserRequest): any {
    const reqHeaders = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http.post('https://localhost:44313/api/account/login', JSON.stringify(userLogin), {headers: reqHeaders}).pipe(
      map((user: any) => {
        localStorage.setItem('currentUser', JSON.stringify(user));
        this.currentUserSubject.next(user);
        return user;
      }
    ));
  }

  logout(): void {
    localStorage.removeItem('currentUser');

    this.currentUserSubject.next(null);
  }

  getCurrentUser(): CurrentUser | null {

    return this.currentUserSubject.value;
  }
}
