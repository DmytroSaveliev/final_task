import { UserRequest } from './../interfaces/user-request';
import { CurrentUser } from '../interfaces/user-current';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { BehaviorSubject, Observable } from 'rxjs';
import { Status } from '../interfaces/status';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(private http: HttpClient) { }

  getCategories(): any {
    return this.http.get('https://localhost:44313/api/categories').pipe(map(
      (categories: any) => {
        return categories.categories;
      }
    ));
  }

  getSubCategory(categoryId: number, subcategoryId: number): any {
    return this.http.get(`https://localhost:44313/api/categories/${categoryId}/subcategories/${subcategoryId}`);
  }
}
