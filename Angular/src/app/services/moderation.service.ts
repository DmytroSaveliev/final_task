import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { BehaviorSubject, Observable } from 'rxjs';
import { Status } from '../interfaces/status';
import { Product } from '../interfaces/product';
import { ProductStatus } from '../interfaces/product-status';

@Injectable({
  providedIn: 'root'
})
export class ModerationService {

  constructor(private http: HttpClient) { }

  modifyCategory(id: number, value: any): any {
    return this.http.patch('https://localhost:44313/api/moderation/categories/' + id, value);
  }

  modifyProduct(id: number, value: any): any {
    return this.http.patch('https://localhost:44313/api/moderation/products/' + id, value);
  }

  modifySubCategory(categoryId: number, id: number, value: any): any {
    return this.http.patch(`https://localhost:44313/api/moderation/categories/${categoryId}/subcategories/` + id, value);
  }

  getCategories(status: Status | null): any {

    let andStatus = '';
    if (status !== null) {
      andStatus = '?status=' + status.toString();
    }

    return this.http.get('https://localhost:44313/api/moderation/categories' + andStatus).pipe(map(
      (categories: any) => {
        return categories.categories;
      }
    ));
  }

  getProducts(limit: number, page: number, productStatus: ProductStatus): any {
    return this.http.get(`https://localhost:44313/api/moderation/products?limit=${limit}&page=${page}&status=${productStatus}`).pipe(map(
      (response: any) => {

        const products = response.products;
        return products.map((p: any): Product => {
          return {
            id: p.id,
            title: p.title,
            description: p.description,
            userEmail: p.user?.email ?? 'none',
            quality: p.details?.quality ?? 0,
            year: p.details?.year ?? 0,
            status: p.status,
            lastModified: p.lastModified,
            created: p.creationDate,
            expiration: p.expirationDate,
            betConfiguration: null
          };
        });
      }
    ));
  }
}
