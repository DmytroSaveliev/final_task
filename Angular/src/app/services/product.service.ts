import { BetConfiguration } from './../interfaces/bet-configuration';
import { ModerationService } from './moderation.service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Product } from '../interfaces/product';
import { map } from 'rxjs/operators';
import { Bet } from '../interfaces/bet';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http: HttpClient) { }

  addProduct(product: any): any {
    return this.http.post(`https://localhost:44313/api/products`, product);
  }

  getProducts(
    limit: number,
    page: number,
    subCategoryId: number | null = null): any {

    let subjects = '';
    if (subCategoryId != null) {
      subjects += '&subCategoryId=' + subCategoryId;
    }

    return this.http.get(`https://localhost:44313/api/products?limit=${limit}&page=${page}${subjects}`).pipe(map(
      (response: any) => {
        return {
          count: response.count,
          products: response.products.map((p: any): Product => {
            return {
              id: p.id,
              title: p.title,
              description: p.description,
              userEmail: p.user?.email ?? 'none',
              quality: p.details?.quality ?? 0,
              year: p.details?.year ?? 0,
              status: p.status,
              lastModified: p.lastModified,
              created: p.creationDate,
              expiration: p.expirationDate,
              betConfiguration: null
            };
          })
        };
      }
    ));
  }

  getBets(productId: number): any {
    return this.http.get(`https://localhost:44313/api/products/${productId}/bets`).pipe(map(
      (response: any) => {
        return response.bets.map((p: any): Bet => {
          return {
            userEmail: p.user?.email ?? 'unknown',
            date: p.date,
            value: p.value
          };
        });
      })
    );
  }

  postBet(productId: number, value: number, productLastModified: string): any {
    return this.http.post(`https://localhost:44313/api/products/${productId}/bets`, { value, productLastModified });
  }

  getProduct(id: number): any {
    return this.http.get(`https://localhost:44313/api/products/${id}`).pipe(map(
      (p: any): Product => {
        return {
          id: p.id,
          title: p.title,
          description: p.description,
          userEmail: p.user?.email ?? 'none',
          quality: p.details?.quality ?? 0,
          year: p.details?.year ?? 0,
          status: p.status,
          lastModified: p.lastModified,
          created: p.creationDate,
          expiration: p.expirationDate,
          betConfiguration: p.betConfiguration
        };
      })
    );
  }
}
