﻿using System;

namespace BLL.DTO
{
    public class BaseDto
    {
        public int Id { get; set; }
        public DateTime LastModified { get; set; }
    }
}
