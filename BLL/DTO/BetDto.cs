﻿
using BLL.Validation.Attributes.Implementation;
using DAL.Entities;
using System;
using System.Collections.Generic;

namespace BLL.DTO
{
    public class BetsDto
    {
        public int Count { get; set; }

        public IEnumerable<BetDto> Bets { get; set; }
    }

    public class BetDto
    {
        public UserDto User { get; set; }

        public DateTime Date { get; set; }

        [Between(0, (double)(AuctionConfiguration.MAX_VALUE + 1), "Invalid value range")]
        public decimal Value { get; set; }
    }

    public class NewBetDto
    {
        [Between(0, (double)(AuctionConfiguration.MAX_VALUE + 1), "Invalid value range")]
        public decimal Value { get; set; }
        public DateTime ProductLastModified { get; set; }
    }
}
