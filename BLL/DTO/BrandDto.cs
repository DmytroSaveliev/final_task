﻿
using BLL.Validation.Attributes.Implementation;
using System.Collections.Generic;

namespace BLL.DTO
{
    public class BrandsDto
    {
        public int Count { get; set; }

        public IEnumerable<BrandDto> Brands { get; set; }
    }

    public class BrandDto : BaseDto
    {
        [NotNull("Brand name can`t be null")]
        [StringLength(2, 55, "Brand name length must be in range 2 - 55")]
        [ValidNameCharacters("Brand name contains invalid characters")]
        public string Name { get; set; }

        [ValidEnumValue("Status value must be in range 0 - 2")]
        public StatusDto Status { get; set; }
    }

    public class NewBrandDto
    {
        [NotNull("Brand name can`t be null")]
        [StringLength(2, 55, "Brand name length must be in range 2 - 55")]
        [ValidNameCharacters("Brand name contains invalid characters")]
        public string Name { get; set; }
    }
}
