﻿
using BLL.Validation.Attributes.Implementation;
using System.Collections.Generic;

namespace BLL.DTO
{
    public class CategoriesFullDto
    {
        public int Count { get; set; }

        public IEnumerable<CategoryFullDto> Categories { get; set; }
    }

    public class CategoriesDto
    {
        public int Count { get; set; }

        public IEnumerable<CategoryDto> Categories { get; set; }
    }

    public class CategoryFullDto : BaseDto
    {
        [NotNull("Category name can`t be null")]
        [StringLength(2, 55, "Category name length must be in range 2 - 55")]
        [ValidNameCharacters("Category name contains invalid characters")]
        public string Name { get; set; }

        public IEnumerable<SubCategoryDto> SubCategories { get; set; }

        [ValidEnumValue("Status value must be in range 0 - 2")]
        public StatusDto Status { get; set; }
    }

    public class CategoryDto : BaseDto
    {
        [NotNull("Category name can`t be null")]
        [StringLength(2, 55, "Category name length must be in range 2 - 55")]
        [ValidNameCharacters("Category name contains invalid characters")]
        public string Name { get; set; }

        [ValidEnumValue("Status value must be in range 0 - 2")]
        public StatusDto Status { get; set; }
    }

    public class NewCategoryDto
    {
        [NotNull("Category name can`t be null")]
        [StringLength(2, 55, "Category name length must be in range 2 - 55")]
        [ValidNameCharacters("Category name contains invalid characters")]
        public string Name { get; set; }

        public IList<string> SubCategoriesName { get; set; }
    }
}
