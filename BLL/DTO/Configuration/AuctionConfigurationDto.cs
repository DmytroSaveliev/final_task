﻿using BLL.Validation.Attributes.Implementation;
using DAL.Entities;
using System.Collections.Generic;

namespace BLL.DTO
{
    public class AuctionConfigurationsDto
    {
        public int Count { get; set; }

        public IEnumerable<AuctionConfigurationDto> AuctionConfigurations { get; set; }
    }

    public class AuctionConfigurationDto : BaseDto
    {
        [GreaterThanZero("Max auction period must be greater than zero")]
        [GreaterThanInner("MinAuctionPeriod", "Max auction period must be greater than min auction period")]
        [LowerThanInner("MaxPeriod", "Max auction period must be lower than max period")]
        public int MaxAuctionPeriod { get; set; }

        [GreaterThanZero("Min auction period must be greater than zero")]
        public int MinAuctionPeriod { get; set; }

        [GreaterThanZero("Max bet capacity must be greater than zero")]
        [GreaterThanInner("MinBetCapacity", "Max bet capacity must be greater than min bet capacity")]
        [LowerThanInner("MaxValue", "Max bet capacity must be lower than max value")]
        public decimal MaxBetCapacity { get; set; }

        [GreaterThanZero("Min bet capacity must be greater than zero")]
        [LowerThanInner("MaxValue", "Max bet capacity must be lower than max value")]
        public decimal MinBetCapacity { get; set; }

        [GreaterOrEqualZero("Bet prolongation duration must be greater or equal zero")]
        public int BetProlongationDuration { get; set; }

        [GreaterOrEqualZero("Auction prolongation period must be greater or equal zero")]
        [GreaterOrEqualThanInner("BetProlongationDuration", "Auction prolongation period must be greater or equal bet prolongation duration")]
        [LowerThanInner("MaxPeriod", "Auction prolongation period must be lower than max period")]
        public int AuctionProlongationPeriod { get; set; }

        public decimal MaxValue { get; } = MAX_VALUE;

        public decimal MaxPeriod { get; } = MAX_PERIOD;

        public readonly static decimal MAX_VALUE = AuctionConfiguration.MAX_VALUE;

        public readonly static int MAX_PERIOD = AuctionConfiguration.MAX_PERIOD;

        public bool? IsActive { get; set; }
    }
}