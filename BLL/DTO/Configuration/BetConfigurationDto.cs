﻿
using BLL.Validation.Attributes.Implementation;

namespace BLL.DTO
{
    public class BetConfigurationDto
    {
        [GreaterThanZero("Buy it now price must be greater than zero")]
        [GreaterThanInner("StartPrice", "Buy it now price must be greater than start price")]
        [LowerOrEqualThanInner("MaxValue", "Buy it now price must be lower or equal max value")]
        public decimal? BuyItNowPrice { get; set; }

        [GreaterThanZero("Min price must be greater than zero")]
        [LowerThanInner("MaxValue", "Min price must be lower than max value")]
        public decimal? MinPrice { get; set; }

        [GreaterOrEqualZero("Start price must be greater or equal zero")]
        [LowerThanInner("MaxValue", "Start price must be lower than max value")]
        public decimal StartPrice { get; set; }

        [GreaterThanZero("Auction period must be greater than zero")]
        public int AuctionPeriod { get; set; }

        [GreaterOrEqualZero("Min bet capacity must be greater or equal zero")]
        public decimal MinBetCapacity { get; set; }

        [GreaterThanZero("Max bet capacity must be greater than zero")]
        [GreaterThanInner("MinBetCapacity", "Max bet capacity must be greater than min bet capacity")]
        public decimal? MaxBetCapacity { get; set; }
        public decimal MaxValue { get; } = AuctionConfigurationDto.MAX_VALUE;
    }
}
