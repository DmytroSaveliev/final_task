﻿
namespace BLL.DTO
{
    public enum ProductConditionDto : byte
    {
        NewPacked,
        NewUnpacked,
        UsedPerfect,
        UsedGood,
        UsedShabby,
        Unusable
    }
}
