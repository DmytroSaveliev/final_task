﻿
namespace BLL.DTO
{
    public enum ProductStatusDto : byte
    {
        New,
        Public,
        Completed,
        Expired,
        Deleted
    }
}
