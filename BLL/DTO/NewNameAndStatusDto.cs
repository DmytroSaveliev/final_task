﻿using BLL.Validation.Attributes.Implementation;
using System;

namespace BLL.DTO
{
    public class NewNameAndStatusDto
    {
        [StringLength(2, 55, "Name length must be in range 2 - 55")]
        [ValidNameCharacters("Name contains invalid characters")]
        public string Name { get; set; }

        [ValidEnumValue("Status value must be in range 0 - 2")]
        public StatusDto? Status { get; set; }

        public DateTime LastModified { get; set; }
    }
}
