﻿
using BLL.Validation.Attributes.Implementation;

namespace BLL.DTO
{
    public class ProductDetailsDto
    {
        [ValidEnumValue("Quality value must be in range 0 - 5")]
        public ProductConditionDto Quality { get; set; }

        [ValidYearTillNow(1000, "Year has invalid range")]
        public int Year { get; set; }
    }
}
