﻿
using BLL.Validation.Attributes.Implementation;
using System;
using System.Collections.Generic;

namespace BLL.DTO
{
    public class ProductsDto
    {
        public int Count { get; set; }
        public IEnumerable<ProductDto> Products { get; set; }
    }

    public class ProductDto : BaseDto
    {
        [NotNull("Product title can`t be null")]
        [StringLength(5, 255, "Product title length must be in range 5 - 255")]
        [ValidNameCharacters("Product title contains invalid characters")]
        public string Title { get; set; }

        [NotNull("Product description can`t be null")]
        [StringLength(5, 16384, "Product description length must be in range 5 - 16384")]
        public string Description { get; set; }

        public BrandDto Brand { get; set; }

        public SubCategoryDto SubCategory { get; set; }

        public UserDto User { get; set; }

        public ProductDetailsDto Details { get; set; }

        public BetConfigurationDto BetConfiguration { get; set; }

        [ValidEnumValue("Product status value must be in range 0 - 4")]
        public ProductStatusDto Status { get; set; }

        public DateTime CreationDate { get; set; }

        public DateTime ExpirationDate { get; set; }
    }

    public class NewProductDto
    {
        [NotNull("Product title can`t be null")]
        [StringLength(5, 255, "Product title length must be in range 5 - 255")]
        [ValidNameCharacters("Product title contains invalid characters")]
        public string Title { get; set; }

        [NotNull("Product description can`t be null")]
        [StringLength(5, 16384, "Product description length must be in range 5 - 16384")]
        public string Description { get; set; }

        [StringLength(2, 55, "Brand name length must be in range 2 - 55")]
        [ValidNameCharacters("Brand name contains invalid characters")]
        public string BrandName { get; set; }

        [StringLength(2, 55, "Subcategory name length must be in range 2 - 55")]
        [ValidNameCharacters("Subcategory name contains invalid characters")]
        public string SubCategoryName { get; set; }

        [NotNull("Product details can`t be null")]
        public ProductDetailsDto Details { get; set; }

        [NotNull("Bet configuration can`t be null")]
        public BetConfigurationDto BetConfiguration { get; set; }
    }

    public class UpdatedProductDto
    {
        [StringLength(5, 255, "Product title length must be in range 5 - 255")]
        [ValidNameCharacters("Product title contains invalid characters")]
        public string Title { get; set; }

        [StringLength(5, 16384, "Product description length must be in range 5 - 16384")]
        public string Description { get; set; }

        [StringLength(2, 55, "Brand name length must be in range 2 - 55")]
        [ValidNameCharacters("Brand name contains invalid characters")]
        public string BrandName { get; set; }

        [StringLength(2, 55, "Subcategory name length must be in range 2 - 55")]
        [ValidNameCharacters("Subcategory name contains invalid characters")]
        public string SubCategoryName { get; set; }

        [ValidEnumValue("Product status value must be in range 0 - 4")]
        public ProductStatusDto? Status { get; set; }

        public DateTime LastModified { get; set; }
    }
}
