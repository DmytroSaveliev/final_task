﻿using System.Collections.Generic;

namespace BLL.DTO
{
    public class SeedDto
    {
        public string AdminEmail { get; set; }

        public string AdminPassword { get; set; }

        public AuctionConfigurationDto AuctionConfiguration { get; set; }

        public IEnumerable<string> Brands { get; set; }

        public IDictionary<string, IList<string>> Categories { get; set; }
    }
}
