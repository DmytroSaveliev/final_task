﻿using BLL.DTO.Selectors.Interface;
using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BLL.DTO
{
    public class BrandSelector : ISelector
    {
        internal protected int? Id { get; set; }
        internal protected Status? Status { get; set; }

        public BrandSelector(StatusDto? status = null)
        {
            if (status == null)
                return;

            Status = Enum.Parse<Status>(status.Value.ToString());
        }

        public BrandSelector(int id, StatusDto? status = null)
            : this(status)
        {
            Id = id;
        }

        public async Task<IEnumerable<T>> SelectAsync<T>(IEnumerable<T> entities) where T : class
        {
            if (IsTypeOf(typeof(T)))
                return await Task.Run(() => (IEnumerable<T>)Select((IEnumerable<Brand>)entities));

            return await Task.Run(() => Enumerable.Empty<T>());
        }

        public async Task<int> CountAsync<T>(IEnumerable<T> entities) where T : class
        {
            if (IsTypeOf(typeof(T)))
                return await Task.Run(() => Count((IEnumerable<Brand>)entities));

            return await Task.Run(() => 0);
        }

        internal virtual int Count(IEnumerable<Brand> entities)
        {
            if (Id.HasValue)
                entities = entities.Where(x => x.Id == Id.Value);

            if (Status.HasValue)
                entities = entities.Where(x => x.Status == Status.Value);

            return entities.Count();
        }

        internal virtual IEnumerable<Brand> Select(IEnumerable<Brand> entities)
        {
            if (Id.HasValue)
                entities = entities.Where(x => x.Id == Id.Value);

            if (Status.HasValue)
                entities = entities.Where(x => x.Status == Status.Value);

            return entities;
        }

        public bool IsTypeOf(Type type) => type == typeof(Brand);
    }
}
