﻿using BLL.DTO.Selectors.Interface;
using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BLL.DTO
{
    public class ProductSelector : ISelector
    {
        internal protected int? Id { get; set; }
        internal protected int? Take { get; set; }
        internal protected int? Skip { get; set; }
        internal protected int? CategoryId { get; set; }
        internal protected int? SubCategoryId { get; set; }
        internal protected int? BrandId { get; set; }
        internal protected IEnumerable<ProductStatus> Statuses { get; set; }

        public ProductSelector(int? limit, int? page, ProductStatusDto? status)
        {
            if (page < 1) page = 1;
            if (limit < 1) limit = 1;

            Take = limit;

            if (limit.HasValue && page.HasValue)
                Skip = (page.Value - 1) * limit.Value;

            if (status.HasValue)
                Statuses = new ProductStatus[] { Enum.Parse<ProductStatus>(status.ToString()) };
        }

        public ProductSelector(int? limit, int? page, ProductStatusDto? status, int? subCategoryId, int? brandId)
            : this(limit, page, status)
        {
            SubCategoryId = subCategoryId;
            BrandId = brandId;
        }

        public ProductSelector(int id, ProductStatusDto? status, int? subCategoryId, int? brandId)
            : this(null, null, status, subCategoryId, brandId)
        {
            Id = id;
        }

        public ProductSelector(int id, IEnumerable<ProductStatusDto> statuses, int? subCategoryId, int? brandId)
            : this(null, null, null, subCategoryId, brandId)
        {
            Id = id;

            if (statuses != null)
            {
                var productStatuses = new List<ProductStatus>();
                foreach(var status in statuses)
                    productStatuses.Add(Enum.Parse<ProductStatus>(status.ToString()));

                Statuses = productStatuses;
            }
        }

        public async Task<IEnumerable<T>> SelectAsync<T>(IEnumerable<T> entities) where T : class
        {
            if (IsTypeOf(typeof(T)))
                return await Task.Run(() => (IEnumerable<T>)Select((IEnumerable<Product>)entities));

            return await Task.Run(() => Enumerable.Empty<T>());
        }

        public async Task<int> CountAsync<T>(IEnumerable<T> entities) where T : class
        {
            if (IsTypeOf(typeof(T)))
                return await Task.Run(() => Count((IEnumerable<Product>)entities));

            return await Task.Run(() => 0);
        }

        internal virtual IEnumerable<Product> Select(IEnumerable<Product> entities)
        {
            if (Id.HasValue)
                entities = entities.Where(x => x.Id == Id.Value);

            if (SubCategoryId.HasValue)
                entities = entities.Where(p => p.SubCategoryId == SubCategoryId.Value);

            if (BrandId.HasValue)
                entities = entities.Where(p => p.BrandId == BrandId.Value);

            if (Statuses?.Any() ?? false)
                entities = entities.Where(p => Statuses.Contains(p.Status));

            if (Skip.HasValue)
                entities = entities.Skip(Skip.Value);

            if (Take.HasValue)
                entities = entities.Take(Take.Value);

            return entities.ToList();
        }

        internal virtual int Count(IEnumerable<Product> entities)
        {
            if (Id.HasValue)
                entities = entities.Where(x => x.Id == Id.Value);

            if (SubCategoryId.HasValue)
                entities = entities.Where(p => p.SubCategoryId == SubCategoryId.Value);

            if (BrandId.HasValue)
                entities = entities.Where(p => p.BrandId == BrandId.Value);

            if (Statuses?.Any() ?? false)
                entities = entities.Where(p => Statuses.Contains(p.Status));

            return entities.Count();
        }

        public bool IsTypeOf(Type type) => type == typeof(Product);
    }
}
