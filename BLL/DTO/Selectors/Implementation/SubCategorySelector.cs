﻿using BLL.DTO.Selectors.Interface;
using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BLL.DTO
{
    public class SubCategorySelector : ISelector
    {
        internal protected int? Id { get; set; }
        internal protected Status? Status { get; set; }
        internal protected int? CategoryId { get; set; }

        public SubCategorySelector(int? categoryId, StatusDto? status = null)
        {
            CategoryId = categoryId;

            if (status.HasValue)
                Status = Enum.Parse<Status>(status.Value.ToString());
        }

        public SubCategorySelector(int id, int? categoryId, StatusDto? status = null)
            : this(categoryId, status)
        {
            Id = id;
        }

        public async Task<IEnumerable<T>> SelectAsync<T>(IEnumerable<T> entities) where T : class
        {
            if (IsTypeOf(typeof(T)))
                return await Task.Run(() => (IEnumerable<T>)Select((IEnumerable<SubCategory>)entities));

            return await Task.Run(() => Enumerable.Empty<T>());
        }

        public async Task<int> CountAsync<T>(IEnumerable<T> entities) where T : class
        {
            if (IsTypeOf(typeof(T)))
                return await Task.Run(() => Count((IEnumerable<SubCategory>)entities));

            return await Task.Run(() => 0);
        }

        internal virtual IEnumerable<SubCategory> Select(IEnumerable<SubCategory> entities)
        {
            if (Id.HasValue)
                entities = entities.Where(x => x.Id == Id.Value);

            if (Status.HasValue)
                entities = entities.Where(x => x.Status == Status.Value);

            if (CategoryId.HasValue)
                entities = entities.Where(x => x.CategoryId == CategoryId.Value);

            return entities;
        }

        internal virtual int Count(IEnumerable<SubCategory> entities)
        {
            if (Id.HasValue)
                entities = entities.Where(x => x.Id == Id.Value);

            if (Status.HasValue)
                entities = entities.Where(p => p.Status == Status.Value);

            if (CategoryId.HasValue)
                entities = entities.Where(x => x.CategoryId == CategoryId.Value);

            return entities.Count();
        }

        public bool IsTypeOf(Type type) => type == typeof(SubCategory);
    }
}
