﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL.DTO.Selectors.Interface
{
    public interface ISelector
    {
        Task<IEnumerable<T>> SelectAsync<T>(IEnumerable<T> entities) where T : class;
        Task<int> CountAsync<T>(IEnumerable<T> entities) where T : class;
        bool IsTypeOf(Type type);
    }


}
