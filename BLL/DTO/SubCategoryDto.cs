﻿
using BLL.Validation.Attributes.Implementation;
using System.Collections.Generic;

namespace BLL.DTO
{
    public class SubCategoriesFullDto
    { 
        public int Count { get; set; }

        public IEnumerable<SubCategoryFullDto> SubCategories { get; set; }
    }

    public class SubCategoryDto : BaseDto
    {
        [NotNull("Subcategory name can`t be null")]
        [StringLength(2, 55, "Subcategory name length must be in range 2 - 55")]
        [ValidNameCharacters("Subcategory name contains invalid characters")]
        public string Name { get; set; }

        [ValidEnumValue("Status value must be in range 0 - 2")]
        public StatusDto Status { get; set; }
    }

    public class SubCategoryFullDto : BaseDto
    {
        [NotNull("Subcategory name can`t be null")]
        [StringLength(2, 55, "Subcategory name length must be in range 2 - 55")]
        [ValidNameCharacters("Subcategory name contains invalid characters")]
        public string Name { get; set; }

        public CategoryDto Category { get; set; }

        [ValidEnumValue("Status value must be in range 0 - 2")]
        public StatusDto Status { get; set; }
    }

    public class NewSubCategoryDto
    {
        [NotNull("Subcategory name can`t be null")]
        [StringLength(2, 55, "Subcategory name length must be in range 2 - 55")]
        [ValidNameCharacters("Subcategory name contains invalid characters")]
        public string Name { get; set; }
    }
}
