﻿
namespace BLL.DTO
{
    public class UserDto : BaseDto
    {
        public string Email { get; set; }
    }
}
