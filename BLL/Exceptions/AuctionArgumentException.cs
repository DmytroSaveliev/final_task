﻿using System;

using System.Runtime.Serialization;

namespace BLL.Exceptions
{
    [Serializable]
    public class AuctionArgumentException : ArgumentException
    {
        public AuctionArgumentException(string message, string paramName)
            : base(message, paramName)
        { }

        protected AuctionArgumentException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        { }
    }
}
