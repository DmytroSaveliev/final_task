﻿using System;

using System.Runtime.Serialization;

namespace BLL.Exceptions
{
    [Serializable]
    public class AuctionArgumentNullException : ArgumentNullException
    {
        public AuctionArgumentNullException(string paramName)
            : base(paramName)
        { }

        protected AuctionArgumentNullException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        { }
    }
}
