﻿using System;
using System.Text;

namespace BLL.Exceptions
{
    public class NotFoundException : ApplicationException
    {
        public string ResourceName { get; }
        public string IdentifierName { get; }
        public string IdentifierValue { get; }

        public NotFoundException(string resourceName, string identifierName = null, string identifierValue = null)
            : base(BuildMessage(resourceName, identifierName, identifierValue))
        {
            ResourceName = resourceName;
            IdentifierName = identifierName;
            IdentifierValue = identifierValue;
        }

        private static string BuildMessage(string resourceName, string identifierName = null, string identifierValue = null)
        {
            var message = new StringBuilder();

            message.Append(resourceName ?? "Unknown");

            if (identifierName != null)
                message.Append($" with {identifierName}");

            if (identifierValue != null)
                message.Append($" = '{identifierValue}'");

            message.Append(" not found");

            return message.ToString();
        }
    }
}
