﻿using DAL.Extensions;
using DAL.Implementation;
using DAL.Interface;
using Microsoft.Extensions.DependencyInjection;

namespace BLL.Extensions
{
    public static class BusinessServiceCollectionExtension
    {
        public static void AddBusinessServices(this IServiceCollection services)
        {
            services.AddTransient<IUnitOfWork, AuctionUnitOfWork>();
            services.AddDataServices();
        }
    }
}
