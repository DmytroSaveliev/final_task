﻿using BLL.DTO;
using BLL.Exceptions;
using BLL.Interface;
using DAL.Entities;
using DAL.Interface;
using System;
using System.Threading.Tasks;

#pragma warning disable S4457 // Parameter validation in "async"/"await" methods should be wrapped

namespace BLL.Implementation
{
    public class AccountService : IAccountService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public AccountService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task CreateUserAsync(UserDto user)
        {
            if (user == null)
                throw new AuctionArgumentNullException("user");

            var entityUser = _mapper.MapItem<User, UserDto>(user);
            entityUser.LastModified = DateTime.Now;

            await _unitOfWork.User.AddItemAsync(entityUser);
            await _unitOfWork.SaveAsync();
        }
    }
}
