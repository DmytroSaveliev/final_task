﻿using BLL.DTO;
using BLL.Exceptions;
using BLL.Interface;
using BLL.Validation;
using BLL.Validation.Attributes.Interface;
using DAL.Entities;
using DAL.Interface;
using System;
using System.Linq;
using System.Threading.Tasks;

#pragma warning disable S4457 // Parameter validation in "async"/"await" methods should be wrapped

namespace BLL.Implementation
{
    public class AuctionService : IAuctionService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public static AuctionConfigurationDto ActiveActionConfiguration { get; private set; }

        public AuctionService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<AuctionConfigurationDto> GetActiveAuctionConfigurationAsync()
        {
            if (ActiveActionConfiguration != null)
                return ActiveActionConfiguration;

            var activeAuctionConfiguration = (await _unitOfWork.AuctionConfiguration.GetItemsAsync(x => x.IsActive == true)).FirstOrDefault();

            if (activeAuctionConfiguration == null)
                throw new NotFoundException("Active auction configuration");

            ActiveActionConfiguration = _mapper.MapItem<AuctionConfigurationDto, AuctionConfiguration>(activeAuctionConfiguration);

            return ActiveActionConfiguration;
        }

        public async Task<IValidationResultReader> CreateActiveAuctionConfigurationAsync(AuctionConfigurationDto auctionConfiguration)
        {
            if (auctionConfiguration == null)
                throw new ArgumentNullException("auctionConfiguration");

            auctionConfiguration.Id = 0;
            auctionConfiguration.IsActive = true;
            auctionConfiguration.LastModified = DateTime.Now;
            var validationResult = Validator.Validate(auctionConfiguration);

            if (!validationResult.IsValid)
                return validationResult;

            var activeAuctionConfiguration = (await _unitOfWork.AuctionConfiguration.GetItemsAsync(x => x.IsActive == true)).FirstOrDefault();
            if (activeAuctionConfiguration != null)
                activeAuctionConfiguration.IsActive = null;

            await _unitOfWork.AuctionConfiguration.AddItemAsync(
                _mapper.MapItem<AuctionConfiguration, AuctionConfigurationDto>(auctionConfiguration)
            );

            await _unitOfWork.SaveAsync();

            ActiveActionConfiguration = auctionConfiguration;

            return new EmptyValidationResult();
        }
    }
}
