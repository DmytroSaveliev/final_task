﻿using BLL.DTO;
using BLL.Exceptions;
using BLL.Interface;
using BLL.Validation;
using BLL.Validation.Attributes.Interface;
using DAL.Entities;
using DAL.Interface;
using System;
using System.Linq;
using System.Threading.Tasks;

#pragma warning disable S4457 // Parameter validation in "async"/"await" methods should be wrapped

namespace BLL.Implementation
{
    public class BetService : IBetService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IAuctionService _auctionService;
        private readonly IWorkerService _workerService;
        public BetService(
            IUnitOfWork unitOfWork, 
            IMapper mapper, 
            IAuctionService auctionService,
            IWorkerService workerService)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _auctionService = auctionService;
            _workerService = workerService;
        }
        public async Task<BetsDto> GetBetsAsync(int productId)
        {
            await _workerService.CloseExpired();

            var product = (await _unitOfWork.Product.GetItemsAsync(
                x => x.Id == productId, 
                x => x.Bets
            )).FirstOrDefault();
            if (product == null)
                throw new NotFoundException("Product", "id", productId.ToString());

            if (product.Status != ProductStatus.Public)
                return new BetsDto { Bets = Enumerable.Empty<BetDto>() };

            return new BetsDto
            {
                Count = product.Bets.Count,
                Bets = _mapper.MapItems<BetDto, Bet>(product.Bets).OrderByDescending(x => x.Date)
            };
        }
        public async Task<IValidationResultReader> CreateBetAsync(string buyerEmail, int productId, NewBetDto newBet)
        {
            if (buyerEmail == null)
                throw new ArgumentNullException("buyerEmail");

            if (newBet == null)
                throw new ArgumentNullException("newBet");

            await _workerService.CloseExpired();

            var product = (await _unitOfWork.Product.GetItemsAsync(
                x => x.Id == productId,
                x => x.BetConfiguration,
                x => x.Bets
            )).FirstOrDefault();

            if (product == null)
                throw new NotFoundException("Product", "id", productId.ToString());

            var buyer = (await _unitOfWork.User.GetItemsAsync(x => x.Email == buyerEmail)).FirstOrDefault();
            if (buyer == null)
                throw new AuctionArgumentException($"User with email '{buyerEmail}' not exist", "buyerEmail");

            var validationResult = new ValidationResult();

            if (product.Status == ProductStatus.Completed)
                validationResult.AddError("Auction completed");

            if (product.Status == ProductStatus.Expired)
                validationResult.AddError("Auction expired");

            if (product.UserId == buyer.Id)
                validationResult.AddError("Product owner is prohibited from placing a bet");

            if (product.LastModified != newBet.ProductLastModified)
                validationResult.AddError("Product has been modified before");

            ValidateBetValue(newBet.Value, product, validationResult);

            if (!validationResult.IsValid)
                return validationResult;

            var buyItNow = product.BetConfiguration.BuyItNowPrice;
            if (buyItNow.HasValue && newBet.Value >= buyItNow.Value)
            {
                product.Status = ProductStatus.Completed;
            }
            else if (newBet.Value == AuctionConfiguration.MAX_VALUE)
            {
                product.Status = ProductStatus.Completed;
            }
            else
            {
                var timeLeft = (product.ExpirationDate - product.CreationDate).TotalSeconds;
                var auctionConfiguration = await _auctionService.GetActiveAuctionConfigurationAsync();

                if (timeLeft <= auctionConfiguration.AuctionProlongationPeriod)
                    product.ExpirationDate = product.ExpirationDate.AddSeconds(auctionConfiguration.BetProlongationDuration);
            }

            var bet = new Bet
            {
                ProductId = product.Id,
                UserId = buyer.Id,
                Value = newBet.Value,
                Date = DateTime.Now,
            };
            product.Bets.Add(bet);
            product.LastModified = DateTime.Now;

            await _unitOfWork.SaveAsync();

            return new EmptyValidationResult();
        }

        private void ValidateBetValue(decimal newValue, Product product, ValidationResult validationResult)
        {
            var betConfig = product.BetConfiguration;
            var lastValue = product.Bets.OrderByDescending(x => x.Date).FirstOrDefault()?.Value ?? product.BetConfiguration.StartPrice;
            var betCapacity = newValue - lastValue;
            bool isBuyItNow = betConfig.BuyItNowPrice.HasValue;

            if (betCapacity <= 0)
                validationResult.AddError("Value", "Bet value must be greater than last bet or start price");

            if (!isBuyItNow)
            {
                if (betConfig.MaxBetCapacity.HasValue && betCapacity > betConfig.MaxBetCapacity)
                    validationResult.AddError("Value", "Bet value must be lower than max bet capacity");

                if (betCapacity < betConfig.MinBetCapacity && newValue != AuctionConfiguration.MAX_VALUE)
                    validationResult.AddError("Value", "Bet value must be greater than min bet capacity");
            }
        }
    }
}
