﻿using BLL.DTO;
using BLL.DTO.Selectors.Interface;
using BLL.Exceptions;
using BLL.Interface;
using BLL.Validation;
using BLL.Validation.Attributes.Interface;
using DAL.Entities;
using DAL.Interface;
using System;
using System.Linq;
using System.Threading.Tasks;

#pragma warning disable S4457 // Parameter validation in "async"/"await" methods should be wrapped

namespace BLL.Implementation
{
    public class BrandService : IBrandService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public BrandService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<BrandsDto> GetBrandsAsync(ISelector selector)
        {
            var brands = _unitOfWork.Brand.GetItems();

            return new BrandsDto
            {
                Count = await selector.CountAsync(brands),
                Brands = _mapper.MapItems<BrandDto, Brand>(await selector.SelectAsync(brands))
            };
        }
        public async Task<BrandDto> GetBrandAsync(ISelector selector)
        {
            var brands = _unitOfWork.Brand.GetItems();
            return _mapper.MapItem<BrandDto, Brand>((await selector.SelectAsync(brands)).FirstOrDefault());
        }
        public async Task<bool> BrandExistAsync(int id)
        {
            return await _unitOfWork.Brand.ExistAsync(x => x.Id == id && x.Status == Status.Publicated);
        }
        public async Task DeleteBrandAsync(int id)
        {
            var brand = await _unitOfWork.Brand.GetItemAsync(id);

            if (brand == null)
                throw new NotFoundException("Brand", "id", id.ToString());

            await _unitOfWork.Brand.RemoveItemAsync(brand);
            await _unitOfWork.SaveAsync();
        }
        public async Task<IValidationResultReader> AddBrandAsync(NewBrandDto newBrand)
        {
            if (newBrand == null)
                throw new AuctionArgumentNullException("newBrand");

            var validationResult = Validator.Validate(newBrand);
            if (!validationResult.IsValid)
                return validationResult;

            if(await _unitOfWork.Brand.ExistAsync(x => x.Name == newBrand.Name))
                return new ValidationResult("Name", $"Brand name '{newBrand.Name}' already exists");

            var brand = new Brand {Name = newBrand.Name, Status = Status.New, LastModified = DateTime.Now};

            await _unitOfWork.Brand.AddItemAsync(brand);
            await _unitOfWork.SaveAsync();

            return new EmptyValidationResult();
        }
        public async Task<IValidationResultReader> ModifyBrandAsync(int id, NewNameAndStatusDto newNameAndStatus)
        {
            if (newNameAndStatus == null)
                throw new AuctionArgumentNullException("newNameAndStatus");

            var validationResult = Validator.Validate(newNameAndStatus);
            if (!validationResult.IsValid)
                return validationResult;

            var brand = await _unitOfWork.Brand.GetItemAsync(id);

            if (brand == null)
                throw new NotFoundException("Brand", "id", id.ToString());

            if (brand.LastModified != newNameAndStatus.LastModified)
                return new ValidationResult("Brand with id = '{id}' has been modified before");

            if (newNameAndStatus.Name != null)
            {
                if (await _unitOfWork.Brand.ExistAsync(x => x.Name == newNameAndStatus.Name))
                    return new ValidationResult("Name", $"Brand name '{newNameAndStatus.Name}' already exists");

                brand.Name = newNameAndStatus.Name;
            }

            if (newNameAndStatus.Status.HasValue)
                brand.Status = Enum.Parse<Status>(newNameAndStatus.Status.Value.ToString());

            brand.LastModified = DateTime.Now;
            await _unitOfWork.SaveAsync();

            return new EmptyValidationResult();
        }
    }
}
