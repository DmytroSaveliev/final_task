﻿using BLL.DTO;
using BLL.DTO.Selectors.Interface;
using BLL.Exceptions;
using BLL.Interface;
using BLL.Validation;
using BLL.Validation.Attributes.Interface;
using DAL.Entities;
using DAL.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

#pragma warning disable S4457 // Parameter validation in "async"/"await" methods should be wrapped

namespace BLL.Implementation
{
    public class CategoryService : ICategoryService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public CategoryService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public async Task<CategoriesDto> GetCategoriesAsync(ISelector selector)
        {
            var categories = _unitOfWork.Category.GetItems();

            return new CategoriesDto
            {
                Count = await selector.CountAsync(categories),
                Categories = _mapper.MapItems<CategoryDto, Category>(await selector.SelectAsync(categories))
            };
        }
        public async Task<CategoriesFullDto> GetCategoriesFullAsync(ISelector selector, StatusDto subcategoryStatus)
        {
            var entityStatus = _mapper.MapItem<Status, StatusDto>(subcategoryStatus);
            var categories = _unitOfWork.Category.GetItems(x => x.SubCategories.Where(x => x.Status == entityStatus));

            return new CategoriesFullDto
            {
                Count = await selector.CountAsync(categories),
                Categories = _mapper.MapItems<CategoryFullDto, Category>(await selector.SelectAsync(categories))
            };
        }
        public async Task<CategoriesFullDto> GetCategoriesFullAsync(ISelector selector)
        {
            var categories = _unitOfWork.Category.GetItems(x => x.SubCategories);

            return new CategoriesFullDto
            {
                Count = await selector.CountAsync(categories),
                Categories = _mapper.MapItems<CategoryFullDto, Category>(await selector.SelectAsync(categories))
            };
        }
        public async Task<SubCategoriesFullDto> GetSubCategoriesAsync(ISelector selector)
        {
            var subCategories = _unitOfWork.SubCategory.GetItems();

            return new SubCategoriesFullDto
            {
                Count = await selector.CountAsync(subCategories),
                SubCategories = _mapper.MapItems<SubCategoryFullDto, SubCategory>(await selector.SelectAsync(subCategories))
            };
        }

        public async Task<CategoryFullDto> GetCategoryAsync(ISelector selector)
        {
            var category = _unitOfWork.Category.GetItems();
            return _mapper.MapItem<CategoryFullDto, Category>((await selector.SelectAsync(category)).FirstOrDefault());
        }
        public async Task<SubCategoryFullDto> GetSubCategoryAsync(ISelector selector)
        {
            var subCategory = _unitOfWork.SubCategory.GetItems(x => x.Category);
            return _mapper.MapItem<SubCategoryFullDto, SubCategory>((await selector.SelectAsync(subCategory)).FirstOrDefault());
        }

        public async Task<bool> CategoryExistAsync(int id, StatusDto status)
        {
            var entityStatus = _mapper.MapItem<Status, StatusDto>(status);
            return await _unitOfWork.Category.ExistAsync(x => x.Id == id && x.Status == entityStatus);
        }

        public async Task<bool> CategoryExistAsync(int id)
        {
            return await _unitOfWork.Category.ExistAsync(x => x.Id == id);
        }

        public async Task<bool> SubCategoryExistAsync(int categoryId, int id)
        {
            return await _unitOfWork.SubCategory.ExistAsync(x => x.Id == id && x.CategoryId == categoryId && x.Status == Status.Publicated);
        }

        public async Task<bool> SubCategoryExistAsync(int categoryId, int id, StatusDto status)
        {
            var entityStatus = _mapper.MapItem<Status, StatusDto>(status);
            return await _unitOfWork.SubCategory.ExistAsync(x => x.Id == id && x.CategoryId == categoryId && x.Status == entityStatus);
        }


        public async Task DeleteCategoryAsync(int id)
        {
            var category = await _unitOfWork.Category.GetItemAsync(id);

            if (category == null)
                throw new NotFoundException("Category", "id", id.ToString());

            await _unitOfWork.Category.RemoveItemAsync(category);
            await _unitOfWork.SaveAsync();
        }
        public async Task DeleteSubCategoryAsync(int categoryId, int id)
        {
            var subCategory = await _unitOfWork.SubCategory.GetItemAsync(id);

            if (subCategory == null)
                throw new NotFoundException("SubCategory", "id", id.ToString());

            if (subCategory.CategoryId != categoryId)
                throw new NotFoundException("SubCategory", "categoryId", categoryId.ToString());

            await _unitOfWork.SubCategory.RemoveItemAsync(subCategory);
            await _unitOfWork.SaveAsync();
        }

        public async Task<IValidationResultReader> AddCategoryAsync(NewCategoryDto newCategory)
        {
            if (newCategory == null)
                throw new AuctionArgumentNullException("newCategory");

            var validationResult = new ValidationResult();
            var categoryDto = await CreateCategoryDto(newCategory, validationResult);

            if (!validationResult.IsValid)
                return validationResult;

            await _unitOfWork.Category.AddItemAsync(_mapper.MapItem<Category, CategoryFullDto>(categoryDto));

            await _unitOfWork.SaveAsync();

            return new EmptyValidationResult();
        }
        public async Task<IValidationResultReader> AddSubCategoryAsync(int categoryId, NewSubCategoryDto newSubCategory)
        {
            if (newSubCategory == null)
                throw new AuctionArgumentNullException("newSubCategory");

            var validationResult = Validator.Validate(newSubCategory);
            if (!validationResult.IsValid)
                return validationResult;

            var category = (await _unitOfWork.Category.GetItemsAsync(
                x => x.Id == categoryId && x.Status == Status.Publicated, 
                x => x.SubCategories)
            ).FirstOrDefault();

            if (category == null)
                throw new NotFoundException("Category", "id", categoryId.ToString());

            if (category.SubCategories.Any(x => x.Name.Contains(newSubCategory.Name, StringComparison.InvariantCultureIgnoreCase)))
                return new ValidationResult("Name", $"Subcategory name '{newSubCategory.Name}' already exists");

            category.SubCategories.Add(
                new SubCategory
                {
                    Name = newSubCategory.Name,
                    LastModified = DateTime.Now,
                    Status = Status.New
                }
            );

            await _unitOfWork.SaveAsync();

            return new EmptyValidationResult();
        }

        public async Task<IValidationResultReader> ModifyCategoryAsync(int id, NewNameAndStatusDto newNameAndStatus)
        {
            if (newNameAndStatus == null)
                throw new AuctionArgumentNullException("newNameAndStatus");

            var validationResult = Validator.Validate(newNameAndStatus);
            if (!validationResult.IsValid)
                return validationResult;

            var category = (await _unitOfWork.Category.GetItemsAsync(x => x.Id == id, x => x.SubCategories)).FirstOrDefault();
            if (category == null)
                throw new NotFoundException("Category", "id", id.ToString());

            if (category.LastModified != newNameAndStatus.LastModified)
                return new ValidationResult($"Category with id = '{id}' has been modified before");

            if (newNameAndStatus.Name != null)
            {
                if (await _unitOfWork.Category.ExistAsync(x => x.Name == newNameAndStatus.Name))
                    return new ValidationResult("Name", $"Category name '{newNameAndStatus.Name}' already exists");

                category.Name = newNameAndStatus.Name;
            }

            var now = DateTime.Now;

            if (newNameAndStatus.Status.HasValue)
            {
                var newStatus = Enum.Parse<Status>(newNameAndStatus.Status.Value.ToString());
                category.Status = newStatus;

                var subCategories = category.SubCategories;
                if (subCategories != null)
                    foreach(var subCategory in category.SubCategories)
                    {
                        subCategory.Status = newStatus;
                        subCategory.LastModified = now;
                    }
            }

            category.LastModified = now;
            await _unitOfWork.SaveAsync();

            return new EmptyValidationResult();
        }
        public async Task<IValidationResultReader> ModifySubCategoryAsync(int categoryId, int id, NewNameAndStatusDto newNameAndStatus)
        {
            if (newNameAndStatus == null)
                throw new AuctionArgumentNullException("newNameAndStatus");

            var validationResult = Validator.Validate(newNameAndStatus);
            if (!validationResult.IsValid)
                return validationResult;

            var subCategory = (await _unitOfWork.SubCategory.GetItemsAsync(x => x.Id == id, x => x.Category)).FirstOrDefault();

            if (subCategory == null)
                throw new NotFoundException($"SubCategory", "id", id.ToString());

            if (subCategory.CategoryId != categoryId)
                throw new NotFoundException($"SubCategory", "categoryId", categoryId.ToString());

            if (subCategory.LastModified != newNameAndStatus.LastModified)
                return new ValidationResult($"Subcategory with id = '{id}' has been modified before");

            if (newNameAndStatus.Name != null)
            {
                if (await _unitOfWork.SubCategory.ExistAsync(x => x.CategoryId == categoryId && x.Name == newNameAndStatus.Name))
                    return new ValidationResult("Name", $"Subcategory name '{newNameAndStatus.Name}' already exists");

                subCategory.Name = newNameAndStatus.Name;
            }

            if (newNameAndStatus.Status.HasValue)
            {
                if (subCategory.Category.Status != Status.Publicated)
                    return new ValidationResult("Status", $"Parent category status must be 'Publicated'");

                subCategory.Status = Enum.Parse<Status>(newNameAndStatus.Status.Value.ToString());
            }

            subCategory.LastModified = DateTime.Now;
            await _unitOfWork.SaveAsync();

            return new EmptyValidationResult();
        }

        private async Task<CategoryFullDto> CreateCategoryDto(NewCategoryDto newCategory, IValidationResultWriter validationResult)
        {
            var now = DateTime.Now;

            var subCategoriesDto = new List<SubCategoryDto>();
            var categoryDto = new CategoryFullDto
            {
                Name = newCategory.Name,
                Status = StatusDto.New,
                SubCategories = subCategoriesDto,
                LastModified = now,
            };

            if (!Validator.Validate(categoryDto).IsValid)
            {
                validationResult.AddError("Category name is invalid");
                return null;
            }

            if (await _unitOfWork.Category.ExistAsync(x => x.Name == categoryDto.Name))
            {
                validationResult.AddError($"Category name '{categoryDto.Name}' already exists");
                return null;
            }

            if (newCategory.SubCategoriesName == null)
                return categoryDto;

            if (newCategory.SubCategoriesName.Count() != newCategory.SubCategoriesName.GroupBy(x => x.ToLower()).Count())
            {
                validationResult.AddError("Subcategory name duplicate");
                return null;
            }

            foreach (var subCategoryName in newCategory.SubCategoriesName)
            {
                var subCategoryDto = new SubCategoryDto
                {
                    Name = subCategoryName,
                    Status = StatusDto.New,
                    LastModified = now,
                };

                if (!Validator.Validate(subCategoryDto).IsValid)
                {
                    validationResult.AddError("Subcategory name is invalid");
                    return null;
                }

                subCategoriesDto.Add(subCategoryDto);
            }

            return categoryDto;
        }
    }
}
