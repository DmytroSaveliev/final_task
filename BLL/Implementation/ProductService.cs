﻿using BLL.DTO;
using BLL.DTO.Selectors.Interface;
using BLL.Exceptions;
using BLL.Interface;
using BLL.Validation;
using BLL.Validation.Attributes.Interface;
using DAL.Entities;
using DAL.Interface;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;

#pragma warning disable S4457 // Parameter validation in "async"/"await" methods should be wrapped

namespace BLL.Implementation
{
    public class ProductService : IProductService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IAuctionService _auctionService;
        private readonly IWorkerService _workerService;

        public ProductService(
            IUnitOfWork unitOfWork, 
            IMapper mapper, 
            IAuctionService auctionService,
            IWorkerService workerService)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _auctionService = auctionService;
            _workerService = workerService;
        }
        public async Task<ProductsDto> GetProductsAsync(ISelector selector)
        {
            if (selector == null)
                throw new AuctionArgumentNullException("selector");

            await _workerService.CloseExpired();

            var products = _unitOfWork.Product.GetItems(
                x => x.User,
                x => x.Details
            );

            return new ProductsDto
            {
                Count = await selector.CountAsync(products),
                Products = _mapper.MapItems<ProductDto, Product>(await selector.SelectAsync(products))
            };
        }
        public async Task<ProductDto> GetProductAsync(ISelector selector, bool includeBetConfiguration)
        {
            if (selector == null)
                throw new AuctionArgumentNullException("selector");

            await _workerService.CloseExpired();

            IEnumerable<Product> products;

            if (includeBetConfiguration)
            {
                products = _unitOfWork.Product.GetItems(
                    x => x.User,
                    x => x.BetConfiguration,
                    x => x.Details
                );
            }
            else
            {
                products = _unitOfWork.Product.GetItems(
                    x => x.User,
                    x => x.Details
                );
            }

            return _mapper.MapItem<ProductDto, Product>((await selector.SelectAsync(products)).FirstOrDefault());
        }
        public async Task<IValidationResultReader> CreateProductAsync(string userEmail, NewProductDto newProduct)
        {
            if (userEmail == null)
                throw new AuctionArgumentNullException("userEmail");

            if (newProduct == null)
                throw new AuctionArgumentNullException("newProduct");

            var validationResult = new ValidationResult();

            Validator.Validate(newProduct, validationResult);
            Validator.Validate(newProduct.Details, validationResult);
            Validator.Validate(newProduct.BetConfiguration, validationResult);

            var product = _mapper.MapItem<Product, NewProductDto>(newProduct);

            product.User = (await _unitOfWork.User.GetItemsAsync(x => x.Email == userEmail)).FirstOrDefault();
            if (product.User == null)
                throw new AuctionArgumentException($"User with email '{userEmail}' not exist", "userEmail");

            await SetProductBrand(product, newProduct.BrandName, validationResult);
            await SetProductSubCategory(product, newProduct.SubCategoryName, validationResult);
            await ValidateProductBetConfiguration(product.BetConfiguration, validationResult);

            if (!validationResult.IsValid)
                return validationResult;

            var date = DateTime.Now;
            product.Status = ProductStatus.New;
            product.LastModified = date;
            product.CreationDate = date;

            await _unitOfWork.Product.AddItemAsync(product);
            await _unitOfWork.SaveAsync();

            return new EmptyValidationResult();
        }
        public async Task<IValidationResultReader> ModifyProductAsync(int id, UpdatedProductDto updatedProduct)
        {
            if (updatedProduct == null)
                throw new AuctionArgumentNullException("updatedProduct");

            await _workerService.CloseExpired();

            var validationResult = new ValidationResult();

            Validator.Validate(updatedProduct, validationResult);
            if (!validationResult.IsValid)
                return validationResult;

            var product = (await _unitOfWork.Product.GetItemsAsync(x => x.Id == id, x => x.BetConfiguration)).FirstOrDefault();
            if (product == null)
                throw new NotFoundException("Product", "id", id.ToString());

            if (product.LastModified != updatedProduct.LastModified)
                validationResult.AddError($"Product with id = '{id}' has been modified before");

            await SetProductBrand(product, updatedProduct.BrandName, validationResult);
            await SetProductSubCategory(product, updatedProduct.SubCategoryName, validationResult);
            SetProductStatus(product, updatedProduct.Status, validationResult);

            if (!validationResult.IsValid)
                return validationResult;

            if (updatedProduct.Title != null)
                product.Title = updatedProduct.Title;

            if (updatedProduct.Description != null)
                product.Description = updatedProduct.Description;

            product.LastModified = DateTime.Now;
            await _unitOfWork.SaveAsync();

            return new EmptyValidationResult();
        }

        private async Task<bool> ValidateProductBetConfiguration(BetConfiguration betConf, IValidationResultWriter validationResult)
        {
            if (betConf == null)
                return false;

            var isValid = true;
            var auctionConf = await _auctionService.GetActiveAuctionConfigurationAsync();

            if (
                betConf.AuctionPeriod < auctionConf.MinAuctionPeriod || 
                betConf.AuctionPeriod > auctionConf.MaxAuctionPeriod
               )
            {
                validationResult.AddError(
                    "AuctionPeriod", 
                    $"Auction period must be in range {auctionConf.MinAuctionPeriod} - {auctionConf.MaxAuctionPeriod}"
                );

                isValid = false;
            }

            if (betConf.MinBetCapacity < auctionConf.MinBetCapacity)
            {
                validationResult.AddError("MinBetCapacity", $"Min bet capacity must be greater or equal {auctionConf.MinBetCapacity}");
                isValid = false;
            }

            if (betConf.MaxBetCapacity > auctionConf.MaxBetCapacity)
            {
                validationResult.AddError("MaxBetCapacity", $"Max bet capacity must be lower or equal {auctionConf.MaxBetCapacity}");
                isValid = false;
            }

            return isValid;
        }
        private async Task SetProductBrand(Product product, string brandName, IValidationResultWriter validationResult)
        {
            if (brandName == null)
                return;

            var brand = (await _unitOfWork.Brand.GetItemsAsync(x => x.Name == brandName && x.Status == Status.Publicated)).FirstOrDefault();

            if (brand == null)
            {
                validationResult.AddError("BrandName", $"Brand with name '{brandName}' not exist");
                return;
            }

            product.Brand = brand;
        }
        private void SetProductStatus(Product product, ProductStatusDto? status, IValidationResultWriter validationResult)
        {
            if (!status.HasValue)
                return;

            if (product.Status != ProductStatus.New)
            {
                validationResult.AddError($"Changing status of not new product is not allowed");
                return;
            }

            var newStatus = Enum.Parse<ProductStatus>(status.Value.ToString());

            if (newStatus != ProductStatus.Deleted && newStatus != ProductStatus.Public)
            {
                validationResult.AddError($"Allowed only Deleted or Public status");
                return;
            }

            product.Status = newStatus;

            if (product.Status == ProductStatus.Public)
            {
                var auctionPeriod = product.BetConfiguration.AuctionPeriod;
                product.ExpirationDate = DateTime.Now.AddSeconds(auctionPeriod);
            }
        }
        private async Task SetProductSubCategory(Product product, string subCategoryName, IValidationResultWriter validationResult)
        {
            if (subCategoryName == null)
                return;

            var subCategory = (await _unitOfWork.SubCategory.GetItemsAsync(
                    x => x.Name == subCategoryName && x.Status == Status.Publicated && x.Category.Status == Status.Publicated)
                ).FirstOrDefault();

            if (subCategory == null)
            {
                validationResult.AddError("SubCategoryName", $"Subcategory with name '{subCategoryName}' not exist");
                return;
            }

            product.SubCategory = subCategory;
        }
    }
}
