﻿using BLL.DTO;
using BLL.Exceptions;
using BLL.Interface;
using BLL.Validation;
using BLL.Validation.Attributes.Interface;
using DAL.Entities;
using DAL.Interface;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BLL.Implementation
{
    public class SeedService : ISeedService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public SeedService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public IValidationResultReader Init(UserDto admin, SeedDto initial)
        {
            if (initial == null)
                throw new AuctionArgumentNullException("initial");

            if (admin == null)
                throw new AuctionArgumentNullException("admin");

            if (!Validator.Validate(admin).IsValid)
                throw new ArgumentException("admin is invalid");

            var now = DateTime.Now;

            admin.LastModified = now;

            var validationResult = new ValidationResult();

            if (initial.AuctionConfiguration == null)
                validationResult.AddError("Auction configuration is required");

            initial.AuctionConfiguration.Id = 0;
            initial.AuctionConfiguration.IsActive = true;
            initial.AuctionConfiguration.LastModified = now;
            Validator.Validate(initial.AuctionConfiguration, validationResult);

            var categories = CreateCategories(initial.Categories, validationResult);
            var brands = CreateBrands(initial.Brands, validationResult);

            if (!validationResult.IsValid)
                return validationResult;

            _unitOfWork.User.AddItemAsync(_mapper.MapItem<User, UserDto>(admin));

            var auctionConfiguration = _mapper.MapItem<AuctionConfiguration, AuctionConfigurationDto>(initial.AuctionConfiguration);
            _unitOfWork.AuctionConfiguration.AddItemAsync(auctionConfiguration);

            foreach (var category in categories)
                _unitOfWork.Category.AddItemAsync(_mapper.MapItem<Category, CategoryFullDto>(category));

            foreach (var brand in brands)
                _unitOfWork.Brand.AddItemAsync(_mapper.MapItem<Brand, BrandDto>(brand));

            _unitOfWork.SaveAsync();

            return new EmptyValidationResult();
        }

        public bool CanInit()
        {
            var propertiesInfo = typeof(IUnitOfWork).GetProperties();

            foreach (var propertyInfo in propertiesInfo)
            {
                dynamic repository = propertyInfo.GetValue(_unitOfWork, null);

                if ((repository.GetItems() as IEnumerable<dynamic>).Any())
                    return false;
            }

            return true;
        }

        private IEnumerable<CategoryFullDto> CreateCategories(
            IDictionary<string, IList<string>> categories,
            IValidationResultWriter validationResult
        )
        {
            var now = DateTime.Now;

            var empty = Enumerable.Empty<CategoryFullDto>();

            if (categories == null)
                return empty;

            var categoriesDto = new List<CategoryFullDto>();

            foreach(var (category, subCategories) in categories)
            {
                var categoryDto = new CategoryFullDto
                {
                    Name = category,
                    Status = StatusDto.Publicated,
                    SubCategories = new List<SubCategoryDto>(),
                    LastModified = now,
                };

                if (!Validator.Validate(categoryDto).IsValid)
                {
                    validationResult.AddError("Category name is invalid");
                    return empty;
                }

                categoriesDto.Add(categoryDto);

                if (subCategories == null)
                    continue;

                if (subCategories.Count() != subCategories.GroupBy(x => x).Count())
                {
                    validationResult.AddError("Subcategory name duplicate");
                    return empty;
                }

                foreach(var subcategory in subCategories)
                {
                    var subCategoryDto = new SubCategoryDto
                    {
                        Name = subcategory,
                        Status = StatusDto.Publicated,
                        LastModified = now
                    };

                    if (!Validator.Validate(subCategoryDto).IsValid)
                    {
                        validationResult.AddError("Subcategory name is invalid");
                        return empty;
                    }

                    ((List<SubCategoryDto>)categoryDto.SubCategories).Add(subCategoryDto);
                }
            }

            return categoriesDto;
        }

        private IEnumerable<BrandDto> CreateBrands(
            IEnumerable<string> brands,
            IValidationResultWriter validationResult
        )
        {
            var empty = Enumerable.Empty<BrandDto>();

            if (brands == null)
                return empty;

            var brandsDto = new List<BrandDto>();

            foreach (var brand in brands)
            {
                var brandDto = new BrandDto
                {
                    Name = brand,
                    Status = StatusDto.Publicated,
                    LastModified = DateTime.Now
                };

                if (!Validator.Validate(brandDto).IsValid)
                {
                    validationResult.AddError("Brand name is invalid");
                    return empty;
                }

                brandsDto.Add(brandDto);
            }

            return brandsDto;
        }
    }
}
