﻿using BLL.Interface;
using DAL.Entities;
using DAL.Interface;
using System;
using System.Linq;
using System.Threading.Tasks;

#pragma warning disable S4457 // Parameter validation in "async"/"await" methods should be wrapped

namespace BLL.Implementation
{
    public class WorkerService : IWorkerService
    {
        private readonly IUnitOfWork _unitOfWork;

        public WorkerService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task CloseExpired()
        {
            var expiredProducts = _unitOfWork.Product.GetItems(
                x => x.Status == ProductStatus.Public && x.ExpirationDate <= DateTime.Now,
                x => x.BetConfiguration,
                x => x.Bets.OrderByDescending(x => x.Date).Take(1)
            );

            foreach (var product in expiredProducts)
                SetProductStatus(product);

            await _unitOfWork.SaveAsync();
        }

        private void SetProductStatus(Product product)
        {
            var lastBet = product.Bets.FirstOrDefault();

            if (lastBet == null)
            {
                product.Status = ProductStatus.Expired;
                return;
            }

            var minPrice = product.BetConfiguration.MinPrice;
            if (!minPrice.HasValue)
            {
                product.Status = ProductStatus.Completed;
                return;
            }

            product.Status = lastBet.Value < minPrice ? 
                ProductStatus.Expired : 
                ProductStatus.Completed;
        }
    }
}
