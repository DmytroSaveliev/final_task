﻿using System.Collections.Generic;
using AutoMapper;
using BLL.DTO;
using DAL.Entities;

namespace BLL.Infrastructure
{
    public class Mapper : Interface.IMapper
    {
        private static readonly MapperConfiguration _config = new MapperConfiguration(cfg => {

            cfg.CreateMap<Product, ProductDto>();
            cfg.CreateMap<ProductDto, Product>();

            cfg.CreateMap<NewProductDto, Product>()
                .ForMember(e => e.BetConfiguration, opt => opt.MapFrom(x => x.BetConfiguration))
                .ForMember(e => e.Description, opt => opt.MapFrom(x => x.Description))
                .ForMember(e => e.Details, opt => opt.MapFrom(x => x.Details))
                .ForMember(e => e.Title, opt => opt.MapFrom(x => x.Title));

            cfg.CreateMap<ProductDetails, ProductDetailsDto>();
            cfg.CreateMap<ProductDetailsDto, ProductDetails>();

            cfg.CreateMap<BetConfiguration, BetConfigurationDto>();
            cfg.CreateMap<BetConfigurationDto, BetConfiguration>();

            cfg.CreateMap<Category, CategoryDto>();
            cfg.CreateMap<CategoryDto, Category>();

            cfg.CreateMap<Category, CategoryFullDto>();
            cfg.CreateMap<CategoryFullDto, Category>();

            cfg.CreateMap<Brand, BrandDto>();
            cfg.CreateMap<BrandDto, Brand>();

            cfg.CreateMap<SubCategory, SubCategoryDto>();
            cfg.CreateMap<SubCategoryDto, SubCategory>();

            cfg.CreateMap<SubCategory, SubCategoryFullDto>();
            cfg.CreateMap<SubCategoryFullDto, SubCategory>();

            cfg.CreateMap<User, UserDto>();
            cfg.CreateMap<UserDto, User>();

            cfg.CreateMap<AuctionConfigurationDto, AuctionConfiguration>();
            cfg.CreateMap<AuctionConfiguration, AuctionConfigurationDto>();

            cfg.CreateMap<BetConfigurationDto, BetConfiguration>();
            cfg.CreateMap<BetConfiguration, BetConfigurationDto>();

            cfg.CreateMap<Bet, BetDto>();
            cfg.CreateMap<BetDto, Bet>();

            cfg.CreateMap<Status, StatusDto>();
            cfg.CreateMap<StatusDto, Status>();
        });

        private static readonly AutoMapper.Mapper _mapper = new AutoMapper.Mapper(_config);

        public TDestionation MapItem<TDestionation, TSourse>(TSourse sourse)
        {
            return _mapper.Map<TDestionation>(sourse);
        }

        public IEnumerable<TDestionation> MapItems<TDestionation, TSourse>(IEnumerable<TSourse> sourse)
        {
            return _mapper.Map<IEnumerable<TDestionation>>(sourse);
        }
    }
}
