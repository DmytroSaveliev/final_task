﻿using BLL.DTO;
using System.Threading.Tasks;

namespace BLL.Interface
{
    public interface IAccountService
    {
        Task CreateUserAsync(UserDto user);
    }
}
