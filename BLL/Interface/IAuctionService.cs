﻿using BLL.DTO;
using BLL.Validation.Attributes.Interface;
using System.Threading.Tasks;

namespace BLL.Interface
{
    public interface IAuctionService
    {
        Task<AuctionConfigurationDto> GetActiveAuctionConfigurationAsync();
        Task<IValidationResultReader> CreateActiveAuctionConfigurationAsync(AuctionConfigurationDto auctionConfiguration);
    }
}
