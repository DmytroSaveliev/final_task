﻿using BLL.DTO;
using BLL.Validation.Attributes.Interface;
using System.Threading.Tasks;

namespace BLL.Interface
{
    public interface IBetService
    {
        Task<BetsDto> GetBetsAsync(int productId);
        Task<IValidationResultReader> CreateBetAsync(string buyerEmail, int productId, NewBetDto newBet);
    }
}
