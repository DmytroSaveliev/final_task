﻿using BLL.DTO;
using BLL.DTO.Selectors.Interface;
using BLL.Validation.Attributes.Interface;
using System.Threading.Tasks;

namespace BLL.Interface
{
    public interface IBrandService
    {
        Task<BrandsDto> GetBrandsAsync(ISelector selector);
        Task<BrandDto> GetBrandAsync(ISelector selector);
        Task<bool> BrandExistAsync(int id);
        Task<IValidationResultReader> AddBrandAsync(NewBrandDto newBrand);
        Task<IValidationResultReader> ModifyBrandAsync(int id, NewNameAndStatusDto newNameAndStatus);
        Task DeleteBrandAsync(int id);
    }
}
