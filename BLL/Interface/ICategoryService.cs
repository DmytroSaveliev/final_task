﻿using BLL.DTO;
using BLL.DTO.Selectors.Interface;
using BLL.Validation.Attributes.Interface;
using System.Threading.Tasks;

namespace BLL.Interface
{
    public interface ICategoryService
    {
        Task<CategoriesFullDto> GetCategoriesFullAsync(ISelector selector);
        Task<CategoriesFullDto> GetCategoriesFullAsync(ISelector selector, StatusDto subcategoryStatus);
        Task<CategoriesDto> GetCategoriesAsync(ISelector selector);
        Task<CategoryFullDto> GetCategoryAsync(ISelector selector);
        Task<bool> CategoryExistAsync(int id);
        Task<bool> CategoryExistAsync(int id, StatusDto status);
        Task<IValidationResultReader> AddCategoryAsync(NewCategoryDto newCategory);
        Task<IValidationResultReader> ModifyCategoryAsync(int id, NewNameAndStatusDto newNameAndStatus);
        Task DeleteCategoryAsync(int id);

        Task<SubCategoriesFullDto> GetSubCategoriesAsync(ISelector selector);
        Task<SubCategoryFullDto> GetSubCategoryAsync(ISelector selector);
        Task<bool> SubCategoryExistAsync(int categoryId, int id);
        Task<bool> SubCategoryExistAsync(int categoryId, int id, StatusDto status);
        Task<IValidationResultReader> AddSubCategoryAsync(int categoryId, NewSubCategoryDto newSubCategory);
        Task<IValidationResultReader> ModifySubCategoryAsync(int categoryId, int id, NewNameAndStatusDto newNameAndStatus);
        Task DeleteSubCategoryAsync(int categoryId, int id);
    }
}
