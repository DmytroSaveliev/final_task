﻿using System.Collections.Generic;

namespace BLL.Interface
{
    public interface IMapper
    {
        TDestionation MapItem<TDestionation, TSourse>(TSourse sourse);

        IEnumerable<TDestionation> MapItems<TDestionation, TSourse>(IEnumerable<TSourse> sourse);
    }
}