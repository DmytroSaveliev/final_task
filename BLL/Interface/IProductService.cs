﻿using BLL.DTO;
using BLL.DTO.Selectors.Interface;
using BLL.Validation.Attributes.Interface;
using System.Threading.Tasks;

namespace BLL.Interface
{
    public interface IProductService
    {
        Task<ProductsDto> GetProductsAsync(ISelector selector);
        Task<ProductDto> GetProductAsync(ISelector selector, bool includeBetConfiguration);
        Task<IValidationResultReader> CreateProductAsync(string userEmail, NewProductDto newProduct);
        Task<IValidationResultReader> ModifyProductAsync(int id, UpdatedProductDto updatedProduct);
    }
}
