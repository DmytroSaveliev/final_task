﻿using BLL.DTO;
using BLL.Validation.Attributes.Interface;

namespace BLL.Interface
{
    public interface ISeedService
    {
        IValidationResultReader Init(UserDto admin, SeedDto initial);
        bool CanInit();
    }
}
