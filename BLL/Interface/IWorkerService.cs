﻿using System.Threading.Tasks;

namespace BLL.Interface
{
    public interface IWorkerService
    {
        Task CloseExpired();
    }
}
