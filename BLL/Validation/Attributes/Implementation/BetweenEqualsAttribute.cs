﻿using BLL.Validation.Attributes.Interface;
using System;

namespace BLL.Validation.Attributes.Implementation
{
    public class BetweenEqualsAttribute : PropertyValidationAttribute
    {
        private readonly double _from;
        private readonly double _to;
        public BetweenEqualsAttribute(double from, double to, string ErrorMessage) : base(ErrorMessage)
        {
            _from = from;
            _to = to;
        }

        public override bool Validate(object property, object obj)
        {
            if (property == null)
                return true;

            if (property is IConvertible converted)
            {
                var value =  converted.ToDouble(null);
                return value >= _from && value <= _to;
            }

            return false;
        }
    }
}
