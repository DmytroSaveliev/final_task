﻿using BLL.Validation.Attributes.Interface;
using System;

namespace BLL.Validation.Attributes.Implementation
{
    public class GreaterOrEqualZeroAttribute : PropertyValidationAttribute
    {
        public GreaterOrEqualZeroAttribute(string ErrorMessage) : base(ErrorMessage)
        { }

        public override bool Validate(object property, object obj)
        {
            if (property == null)
                return true;

            if (property is IConvertible converted)
                return converted.ToDouble(null) >= 0;

            return false;
        }
    }
}
