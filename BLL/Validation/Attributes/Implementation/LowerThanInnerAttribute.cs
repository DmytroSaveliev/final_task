﻿using BLL.Validation.Attributes.Interface;
using System;

namespace BLL.Validation.Attributes.Implementation
{
    public class LowerThanInnerAttribute : PropertyValidationAttribute
    {
        private readonly string _propertyName;
        public LowerThanInnerAttribute(string propertyName, string ErrorMessage) : base(ErrorMessage)
        {
            _propertyName = propertyName;
        }

        public override bool Validate(object property, object obj)
        {
            if (property == null || obj == null)
                return true;

            if (
                property is IConvertible thisConverted && 
                obj.GetType().GetProperty(_propertyName)?.GetValue(obj) is IConvertible innerConverted
               )
            {
                return thisConverted.ToDouble(null) < innerConverted.ToDouble(null);
            }
                
            return false;
        }
    }
}
