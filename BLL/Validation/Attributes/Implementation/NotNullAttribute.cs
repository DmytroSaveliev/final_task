﻿using BLL.Validation.Attributes.Interface;

namespace BLL.Validation.Attributes.Implementation
{
    public class NotNullAttribute : PropertyValidationAttribute
    {
        public NotNullAttribute(string ErrorMessage) : base(ErrorMessage)
        { }

        public override bool Validate(object property, object obj)
        {
            return property != null;
        }
    }
}
