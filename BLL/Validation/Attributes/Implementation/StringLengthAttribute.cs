﻿using BLL.Validation.Attributes.Interface;
using System;

namespace BLL.Validation.Attributes.Implementation
{
    public class StringLengthAttribute : PropertyValidationAttribute
    {
        private readonly double _from;
        private readonly double _to;
        public StringLengthAttribute(double from, double to, string ErrorMessage) : base(ErrorMessage)
        {
            _from = from;
            _to = to;
        }

        public override bool Validate(object property, object obj)
        {
            if (property == null)
                return true;

            if (property is string value)
            {
                return value.Length >= _from && value.Length <= _to;
            }

            return false;
        }
    }
}
