﻿using BLL.Validation.Attributes.Interface;
using System;

namespace BLL.Validation.Attributes.Implementation
{
    public class ValidEnumValueAttribute : PropertyValidationAttribute
    {
        public ValidEnumValueAttribute(string ErrorMessage) : base(ErrorMessage)
        { }

        public override bool Validate(object property, object obj)
        {
            if (property == null)
                return true;

            return Enum.IsDefined(property.GetType(), property);
        }
    }
}
