﻿using BLL.Validation.Attributes.Interface;
using System.Text.RegularExpressions;

namespace BLL.Validation.Attributes.Implementation
{
    public class ValidNameCharactersAttribute : PropertyValidationAttribute
    {
        const string _pattern = @"^[a-zA-Z0-9 \.\,\:\-]+$";
        public ValidNameCharactersAttribute(string ErrorMessage) : base(ErrorMessage)
        { }

        public override bool Validate(object property, object obj)
        {
            if (property == null)
                return true;

            if (property is string value)
            {
                return Regex.IsMatch(value, _pattern);
            }
                
            return false;
        }
    }
}
