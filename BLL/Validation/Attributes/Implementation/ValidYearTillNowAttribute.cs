﻿using BLL.Validation.Attributes.Interface;
using System;

namespace BLL.Validation.Attributes.Implementation
{
    public class ValidYearTillNowAttribute : PropertyValidationAttribute
    {
        private readonly DateTime _from;
        private readonly DateTime _now;
        public ValidYearTillNowAttribute(DateTime fromYear, string ErrorMessage) : base(ErrorMessage)
        {
            _now = DateTime.Now;
            _from = fromYear;
        }

        public ValidYearTillNowAttribute(int fromYearsAgo, string ErrorMessage) : base(ErrorMessage)
        {
            _now = DateTime.Now;
            _from = _now.AddYears(-fromYearsAgo);
        }

        public override bool Validate(object property, object obj)
        {
            if (property == null)
                return true;

            if (property is DateTime dateTimeValue)
            {
                return dateTimeValue.Year >= _from.Year && dateTimeValue.Year <= _now.Year;
            }
            else if (property is int intValue) 
            {
                return intValue >= _from.Year && intValue <= _now.Year;
            }

            return false;
        }
    }
}
