﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Validation.Attributes.Interface
{
    public interface IValidationResultReader
    {
        bool IsValid { get; }
        IEnumerable<string> GetErrors();
    }
}
