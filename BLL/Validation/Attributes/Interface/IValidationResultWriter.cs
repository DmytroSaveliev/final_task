﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Validation.Attributes.Interface
{
    public interface IValidationResultWriter
    {
        void AddError(string name, string error);
        void AddError(string error);
    }
}
