﻿using System;

namespace BLL.Validation.Attributes.Interface
{
    [AttributeUsage(AttributeTargets.Property)]
    public abstract class PropertyValidationAttribute : Attribute
    {
        protected PropertyValidationAttribute(string ErrorMessage)
        {
            this.ErrorMessage = ErrorMessage;
        }

        public string ErrorMessage { get; set; }

        public abstract bool Validate (object property, object obj);
    }
}
