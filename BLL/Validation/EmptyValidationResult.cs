﻿using BLL.Validation.Attributes.Interface;
using System.Collections.Generic;
using System.Linq;

namespace BLL.Validation
{
    public class EmptyValidationResult : IValidationResultReader
    {
        public bool IsValid { get => true; }

        public IEnumerable<string> this[string name]
        {  
            get => Enumerable.Empty<string>();
        }

        public IEnumerable<string> GetErrors() => Enumerable.Empty<string>();
    }
}
