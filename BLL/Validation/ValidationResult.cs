﻿using BLL.Validation.Attributes.Interface;
using System.Collections.Generic;
using System.Linq;

namespace BLL.Validation
{
    public class ValidationResult : IValidationResultWriter, IValidationResultReader
    {
        private readonly Dictionary<string, LinkedList<string>> _errors;

        public bool IsValid { get => _errors.Count == 0; }

        public ValidationResult()
        {
            _errors = new Dictionary<string, LinkedList<string>>();
        }

        public ValidationResult(string name, string error)
        {
            _errors = new Dictionary<string, LinkedList<string>>
            {
                { name, new LinkedList<string>(new string [] { error })}
            };
        }

        public ValidationResult(string error) : this("", error)
        { }

        public ValidationResult(IEnumerable<string> errors)
        {
            _errors = new Dictionary<string, LinkedList<string>>
            {
                { "", new LinkedList<string>(errors)}
            };
        }

        public void AddError(string name, string error)
        {
            if (!_errors.ContainsKey(name ?? ""))
                _errors.Add(name ?? "", new LinkedList<string>());

            _errors[name].AddLast(error);
        }

        public void AddError(string error) => AddError("", error);

        public IEnumerable<string> this[string name]
        {  
            get
            {
                if (_errors.ContainsKey(name ?? ""))
                    return _errors[name ?? ""];

                return Enumerable.Empty<string>();
            }
        }

        public IEnumerable<string> GetErrors()
        {
            var values = _errors.Values;

            foreach(var valueErrors in values)
            {
                foreach(var valueError in valueErrors)
                {
                    yield return valueError;
                }
            }
        }
    }
}
