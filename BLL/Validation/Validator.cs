﻿using BLL.Validation.Attributes.Interface;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace BLL.Validation
{
    public static class Validator
    {
        private static Dictionary<Type, List<Tuple<PropertyInfo, PropertyValidationAttribute>>> _Properties = 
            new Dictionary<Type, List<Tuple<PropertyInfo, PropertyValidationAttribute>>> ();

        public static void Validate(object obj, IValidationResultWriter validationResult)
        {
            //todo: optimize
            if (obj == null)
                return;

            ValidateProperties(obj, validationResult);
        }

        public static IValidationResultReader Validate(object obj)
        {
            var validationResult = new ValidationResult();

            //todo: optimize
            if (obj == null)
                return validationResult;

            ValidateProperties(obj, validationResult);

            return validationResult;
        }

        public static void ClearChache()
        {
            _Properties = new Dictionary<Type, List<Tuple<PropertyInfo, PropertyValidationAttribute>>>();
        }

        private static void ValidateProperties(object obj, IValidationResultWriter validationResult)
        {
            if (obj == null)
            {
                validationResult.AddError("Null");
                return;
            }

            var type = obj.GetType();

            var propertiesInfo = type.GetProperties();

            if (!_Properties.ContainsKey(type))
                StoreProperties(type, propertiesInfo);

            foreach(var (property, validator) in _Properties[type])
            {
                if (!validator.Validate(property.GetValue(obj), obj))
                {
                    validationResult.AddError(property.Name, validator.ErrorMessage);
                }
            }
        }

        private static void StoreProperties(Type type, PropertyInfo[] propertiesInfo)
        {
            //todo:: set locker
            //todo:: implement nested object validation
            _Properties.Add(type, new List<Tuple<PropertyInfo, PropertyValidationAttribute>>());

            foreach (var propertyInfo in propertiesInfo)
            {
                foreach (var attribute in propertyInfo.GetCustomAttributes(true))
                {
                    if (attribute is PropertyValidationAttribute validationAttribute)
                    {
                        _Properties[type].Add(
                            new Tuple<PropertyInfo, PropertyValidationAttribute>(propertyInfo, validationAttribute)
                        );
                    }
                }
            }
        }
    }
}
