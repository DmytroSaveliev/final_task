﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DAL.Entities
{
    public class Bet
    {
        public int UserId { get; set; }

        [Required]
        public User User { get; set; }

        public int ProductId { get; set; }

        [Required]
        public Product Product { get; set; }

        public DateTime Date { get; set; }

        [Range(0, (double)AuctionConfiguration.MAX_VALUE)]
        public decimal Value { get; set; }
    }
}
