﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DAL.Entities
{
    public class Category : BaseEntity
    {
        [Required]
        [StringLength(55, MinimumLength = 2)]
        public string Name { get; set; }

        public virtual ICollection<SubCategory> SubCategories { get; set; }

        public virtual ICollection<Product> Products { get; set; }

        public Status Status { get; set; }
    }
}
