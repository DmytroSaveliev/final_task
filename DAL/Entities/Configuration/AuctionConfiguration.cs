﻿
namespace DAL.Entities
{
    public class AuctionConfiguration : BaseEntity
    {
        public int MaxAuctionPeriod { get; set; }

        public int MinAuctionPeriod { get; set; }

        public decimal MaxBetCapacity { get; set; }

        public decimal MinBetCapacity { get; set; }

        public int BetProlongationDuration { get; set; }

        public int AuctionProlongationPeriod { get; set; }

        public bool? IsActive { get; set; }

        public const decimal MAX_VALUE = (decimal)int.MaxValue;

        public const int MAX_PERIOD = 31556952; //one year
    }
}
