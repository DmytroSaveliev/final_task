﻿
namespace DAL.Entities
{
    public class BetConfiguration
    {
        public int ProductId { get; set; }

        public Product Product { get; set; }

        public decimal? BuyItNowPrice { get; set; }

        public decimal? MinPrice { get; set; }

        public decimal StartPrice { get; set; }

        public int AuctionPeriod { get; set; }

        public decimal MinBetCapacity { get; set; }

        public decimal? MaxBetCapacity { get; set; }
    }
}
