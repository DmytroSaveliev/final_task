﻿
namespace DAL.Entities
{
    public enum ProductCondition
    {
        NewPacked,
        NewUnpacked,
        UsedPerfect,
        UsedGood,
        UsedShabby,
        Unusable
    }
}
