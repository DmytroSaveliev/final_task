﻿
namespace DAL.Entities
{
    public enum ProductStatus : byte
    {
        New,
        Public,
        Completed,
        Expired,
        Deleted
    }
}
