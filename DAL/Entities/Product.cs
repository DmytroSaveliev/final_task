﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DAL.Entities
{
    public class Product : BaseEntity
    {
        [Required]
        [StringLength(255, MinimumLength = 5)]
        public string Title { get; set; }

        [StringLength(16384, MinimumLength = 5)]
        public string Description { get; set; }

        public int? BrandId { get; set; }
        public Brand Brand { get; set; }

        public int? SubCategoryId { get; set; }
        public SubCategory SubCategory { get; set; }

        public int UserId { get; set; }
        public User User { get; set; }

        [Required]
        public ProductDetails Details { get; set; }

        [Required]
        public BetConfiguration BetConfiguration { get; set; }

        public virtual ICollection<Bet> Bets { get; set; }

        public DateTime CreationDate { get; set; }

        public DateTime ExpirationDate { get; set; }

        public ProductStatus Status { get; set; }
    }
}
