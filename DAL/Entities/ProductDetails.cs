﻿using System.ComponentModel.DataAnnotations;

namespace DAL.Entities
{
    public class ProductDetails
    {
        public int ProductId { get; set; }

        [Required]
        public Product Product { get; set; }

        public ProductCondition Quality { get; set; }

        [Range(1, 9999)]
        public int Year { get; set; }
    }
}
