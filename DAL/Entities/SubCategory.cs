﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DAL.Entities
{
    public class SubCategory : BaseEntity
    {
        [Required]
        [StringLength(55, MinimumLength = 2)]
        public string Name { get; set; }

        public int CategoryId { get; set; }

        [Required]
        public Category Category { get; set; }

        public virtual ICollection<Product> Products { get; set; }

        public Status Status { get; set; }
    }
}
