﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DAL.Entities
{
    public class User : BaseEntity
    {
        [Required]
        public string Email { get; set; }

        public virtual ICollection<Product> Products { get; set; }

        public virtual ICollection<Bet> Bets { get; set; }
    }
}
