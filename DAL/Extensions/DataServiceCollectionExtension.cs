﻿using DAL.Implementation;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace DAL.Extensions
{
    public static class DataServiceCollectionExtension
    {
        public static void AddDataServices(this IServiceCollection services)
        {
            services.AddTransient<DbContext, AuctionDbContext>( 
                (_) => new DesignTimeDbContextFactory().CreateDbContext(new string[] { }) 
            );
        }
    }
}
