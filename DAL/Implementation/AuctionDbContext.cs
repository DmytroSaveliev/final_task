﻿using DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace DAL.Implementation
{
    public class AuctionDbContext : DbContext
    {
        public AuctionDbContext(DbContextOptions<AuctionDbContext> options)
            : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            AddKeys(modelBuilder);
            AddUniques(modelBuilder);
            AddIndexes(modelBuilder);
            AddForeignes(modelBuilder);
        }

        private void AddKeys(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Bet>().HasKey(x => new { x.Date, x.ProductId, x.UserId });
            modelBuilder.Entity<ProductDetails>().HasKey(x => x.ProductId);
            modelBuilder.Entity<BetConfiguration>().HasKey(x => x.ProductId);
        }
        private void AddUniques(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().HasIndex(x => x.Email).IsUnique();
            modelBuilder.Entity<Brand>().HasIndex(x => x.Name).IsUnique();
            modelBuilder.Entity<Category>().HasIndex(x => x.Name).IsUnique();
            modelBuilder.Entity<SubCategory>().HasIndex(x => new { x.CategoryId, x.Name }).IsUnique();
            modelBuilder.Entity<AuctionConfiguration>().HasIndex(x => x.IsActive).IsUnique();
        }
        private void AddIndexes(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Brand>().HasIndex(x => x.Status);
            modelBuilder.Entity<Category>().HasIndex(x => x.Status);
            modelBuilder.Entity<SubCategory>().HasIndex(x => x.Status);
            modelBuilder.Entity<Product>().HasIndex(x => x.Status);
            modelBuilder.Entity<Product>().HasIndex(x => x.ExpirationDate);
        }
        private void AddForeignes(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().HasMany(x => x.Bets).WithOne(x => x.User).OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<Product>().HasOne(x => x.Details).WithOne(x => x.Product).HasForeignKey<ProductDetails>(x => x.ProductId).OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<Product>().HasOne(x => x.BetConfiguration).WithOne(x => x.Product).HasForeignKey<BetConfiguration>(x => x.ProductId).OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<Product>().HasOne(x => x.Brand).WithMany(x => x.Products).OnDelete(DeleteBehavior.SetNull);
            modelBuilder.Entity<Product>().HasOne(x => x.SubCategory).WithMany(x => x.Products).OnDelete(DeleteBehavior.SetNull);
            modelBuilder.Entity<Product>().HasOne(x => x.User).WithMany(x => x.Products).OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<Bet>().HasOne(x => x.Product).WithMany(x => x.Bets).OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<Bet>().HasOne(x => x.User).WithMany(x => x.Bets).OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<Category>().HasMany(x => x.SubCategories).WithOne(x => x.Category).OnDelete(DeleteBehavior.Cascade);
        }
    }

    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<AuctionDbContext>
    {
        public AuctionDbContext CreateDbContext(string[] args)
        {
            var currentDirectory = Directory.GetCurrentDirectory();

            IConfigurationRoot config = new ConfigurationBuilder()
                .SetBasePath(currentDirectory)
                .AddJsonFile(currentDirectory + @"\..\FINAL_TASK\appsettings.json")
                .Build();

            var builder = new DbContextOptionsBuilder<AuctionDbContext>();
            var connectionString = config.GetConnectionString("DefaultConnection");
            builder.UseSqlServer(connectionString);

            return new AuctionDbContext(builder.Options);
        }
    }
}
