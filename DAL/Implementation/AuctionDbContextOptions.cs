﻿using System.IO;
using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore;

namespace DAL.Implementation
{
    public class AuctionDbContextOptionsBuilder : DbContextOptionsBuilder<AuctionDbContext>
    {
        private readonly IConfiguration _configuration;

        public AuctionDbContextOptionsBuilder()
        {
            var configurationBuilder = new ConfigurationBuilder();
            configurationBuilder.SetBasePath(Directory.GetCurrentDirectory());
            configurationBuilder.AddJsonFile("appsettings.json");
            _configuration = configurationBuilder.Build();
        }

        public DbContextOptions<AuctionDbContext> GetOptions() 
        {
            string connectionString = _configuration.GetConnectionString("DefaultConnection");
            return this.UseSqlServer(connectionString).Options;
        }
    }
}
