﻿using DAL.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Linq;
using System.Threading.Tasks;

namespace DAL.Implementation
{
    public class AuctionRepository<T> : IRepository<T> where T : class
    {
        protected readonly DbSet<T> _dbSet;

        public AuctionRepository(DbContext dbContext)
        {
            _dbSet = dbContext.Set<T>();
        }

        public async Task AddItemAsync(T item)
        {
            await _dbSet.AddAsync(item);
        }

        public async Task<T> GetItemAsync(int id)
        {
            return await _dbSet.FindAsync(id);
        }

        public IEnumerable<T> GetItems()
        {
            return _dbSet;
        }

        public IEnumerable<T> GetItems<TResult>(Expression<Func<T, bool>> predicate, Expression<Func<T, TResult>> navigationPropertyPath)
        {
            return _dbSet.Where(predicate).Include(navigationPropertyPath);
        }

        public IEnumerable<T> GetItems<TResult1, TResult2>(
            Expression<Func<T, bool>> predicate,
            Expression<Func<T, TResult1>> navigationPropertyPath1,
            Expression<Func<T, TResult2>> navigationPropertyPath2)
        {
            return _dbSet.Where(predicate).Include(navigationPropertyPath1).Include(navigationPropertyPath2);
        }

        public IEnumerable<T> GetItems<TResult>(Expression<Func<T, TResult>> navigationPropertyPath)
        {
            return _dbSet.Include(navigationPropertyPath);
        }
        public IEnumerable<T> GetItems<TResult1, TResult2>(Expression<Func<T, TResult1>> navigationPropertyPath1, Expression<Func<T, TResult2>> navigationPropertyPath2)
        {
            return _dbSet.Include(navigationPropertyPath1).Include(navigationPropertyPath2);
        }
        public IEnumerable<T> GetItems<TResult1, TResult2, TResult3>(
            Expression<Func<T, TResult1>> navigationPropertyPath1,
            Expression<Func<T, TResult2>> navigationPropertyPath2,
            Expression<Func<T, TResult3>> navigationPropertyPath3)
        {
            return _dbSet
                .Include(navigationPropertyPath1)
                .Include(navigationPropertyPath2)
                .Include(navigationPropertyPath3);
        }
        public IEnumerable<T> GetItems<TResult1, TResult2, TResult3, TResult4>(
            Expression<Func<T, TResult1>> navigationPropertyPath1,
            Expression<Func<T, TResult2>> navigationPropertyPath2,
            Expression<Func<T, TResult3>> navigationPropertyPath3,
            Expression<Func<T, TResult4>> navigationPropertyPath4)
        {
            return _dbSet
                .Include(navigationPropertyPath1)
                .Include(navigationPropertyPath2)
                .Include(navigationPropertyPath3)
                .Include(navigationPropertyPath4);
        }
        public async Task<IEnumerable<T>> GetItemsAsync<TResult>(Expression<Func<T, TResult>> navigationPropertyPath)
        {
            return await _dbSet.Include(navigationPropertyPath).ToListAsync();
        }

        public async Task<IEnumerable<T>> GetItemsAsync(Expression<Func<T, bool>> predicate)
        {
            return await _dbSet.Where(predicate).ToListAsync();
        }

        public async Task<IEnumerable<T>> GetItemsAsync<TResult>(
            Expression<Func<T, bool>> predicate, 
            Expression<Func<T, TResult>> navigationPropertyPath)
        {
            return await _dbSet.Where(predicate).Include(navigationPropertyPath).ToListAsync();
        }

        public async Task<IEnumerable<T>> GetItemsAsync<TResult1, TResult2>(
            Expression<Func<T, bool>> predicate, 
            Expression<Func<T, TResult1>> navigationPropertyPath1, 
            Expression<Func<T, TResult2>> navigationPropertyPath2
        )
        {
            return await _dbSet
                .Where(predicate)
                .Include(navigationPropertyPath1)
                .Include(navigationPropertyPath2)
                .ToListAsync();
        }

        public async Task<int> CountAsync(Expression<Func<T, bool>> predicate)
        {
            return await _dbSet.CountAsync(predicate);
        }

        public async Task<bool> ExistAsync(Expression<Func<T, bool>> predicate)
        {
            return await _dbSet.AnyAsync(predicate);
        }

        public async Task RemoveItemAsync(T item)
        {
            await Task.Run(() => {

                _dbSet.Remove(item);
            }); 
        }

        public async Task UpdateItemAsync(T item)
        {
            await Task.Run(() => {

                _dbSet.Update(item);
            });
        }
    }
}
