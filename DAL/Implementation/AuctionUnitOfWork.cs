﻿using DAL.Entities;
using DAL.Interface;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace DAL.Implementation
{
    public class AuctionUnitOfWork : IUnitOfWork
    {
        private readonly DbContext _dbContext;

        public AuctionUnitOfWork(DbContext dbContext)
        {
            _dbContext = dbContext;

            AuctionConfiguration = new AuctionRepository<AuctionConfiguration>(_dbContext);
            Category = new AuctionRepository<Category>(_dbContext);
            SubCategory = new AuctionRepository<SubCategory>(_dbContext);
            Brand = new AuctionRepository<Brand>(_dbContext);
            Product = new AuctionRepository<Product>(_dbContext);
            User = new AuctionRepository<User>(_dbContext);
        }

        public IRepository<AuctionConfiguration> AuctionConfiguration { get; }

        public IRepository<Category> Category { get; }

        public IRepository<SubCategory> SubCategory { get; }

        public IRepository<Brand> Brand { get; }

        public IRepository<Product> Product { get; }

        public IRepository<User> User { get; }

        public async Task SaveAsync()
        {
            await _dbContext.SaveChangesAsync();
        }
    }
}
