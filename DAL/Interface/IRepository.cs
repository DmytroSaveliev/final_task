﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace DAL.Interface
{
    public interface IRepository<T> where T : class
    {
        Task AddItemAsync(T item);
        Task<T> GetItemAsync(int id);
        IEnumerable<T> GetItems();
        IEnumerable<T> GetItems<TResult>(Expression<Func<T, bool>> predicate, Expression<Func<T, TResult>> navigationPropertyPath);
        IEnumerable<T> GetItems<TResult1, TResult2>(
            Expression<Func<T, bool>> predicate,
            Expression<Func<T, TResult1>> navigationPropertyPath1, 
            Expression<Func<T, TResult2>> navigationPropertyPath2);
        IEnumerable<T> GetItems<TResult>(Expression<Func<T, TResult>> navigationPropertyPath);
        IEnumerable<T> GetItems<TResult1, TResult2>(Expression<Func<T, TResult1>> navigationPropertyPath1, Expression<Func<T, TResult2>> navigationPropertyPath2);
        IEnumerable<T> GetItems<TResult1, TResult2, TResult3>(
            Expression<Func<T, TResult1>> navigationPropertyPath1,
            Expression<Func<T, TResult2>> navigationPropertyPath2,
            Expression<Func<T, TResult3>> navigationPropertyPath3);
        IEnumerable<T> GetItems<TResult1, TResult2, TResult3, TResult4>(
            Expression<Func<T, TResult1>> navigationPropertyPath1,
            Expression<Func<T, TResult2>> navigationPropertyPath2,
            Expression<Func<T, TResult3>> navigationPropertyPath3,
            Expression<Func<T, TResult4>> navigationPropertyPath4);
        Task<IEnumerable<T>> GetItemsAsync<TResult>(Expression<Func<T, TResult>> navigationPropertyPath);
        Task<IEnumerable<T>> GetItemsAsync(Expression<Func<T, bool>> predicate);
        Task<IEnumerable<T>> GetItemsAsync<TResult>(Expression<Func<T, bool>> predicate, Expression<Func<T, TResult>> navigationPropertyPath);
        Task<IEnumerable<T>> GetItemsAsync<TResult1, TResult2>(
            Expression<Func<T, bool>> predicate, 
            Expression<Func<T, TResult1>> navigationPropertyPath1, 
            Expression<Func<T, TResult2>> navigationPropertyPath2
        );
        Task<int> CountAsync(Expression<Func<T, bool>> predicate);
        Task<bool> ExistAsync(Expression<Func<T, bool>> predicate);
        Task RemoveItemAsync(T item);
        Task UpdateItemAsync(T item);
    }
}
