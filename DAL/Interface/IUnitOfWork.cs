﻿using DAL.Entities;
using System.Threading.Tasks;

namespace DAL.Interface
{
    public interface IUnitOfWork
    {
        IRepository<AuctionConfiguration> AuctionConfiguration { get; }

        IRepository<Category> Category { get; }

        IRepository<SubCategory> SubCategory { get; }

        IRepository<Brand> Brand { get; }

        IRepository<Product> Product { get; }

        IRepository<User> User { get; }

        Task SaveAsync();
    }
}
