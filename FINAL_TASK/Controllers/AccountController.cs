﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using BLL.DTO;
using BLL.Exceptions;
using BLL.Interface;
using BLL.Validation;
using FINAL_TASK.Extensions;
using FINAL_TASK.Identity;
using FINAL_TASK.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace FINAL_TASK.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AccountController : ControllerBase
    {
        private readonly IAccountService _accountService;
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly IConfiguration _configuration;

        public AccountController(
            IAccountService accountService,
            UserManager<User> userManager,
            SignInManager<User> signInManager,
            IConfiguration configuration)
        {
            _accountService = accountService;
            _userManager = userManager;
            _signInManager = signInManager;
            _configuration = configuration;
        }

        [HttpPost("users")]
        public async Task<IActionResult> PostUser([FromBody] UserRequest userRequest)
            => await CreateUserInternalAsync(userRequest, "User");

        [Authorize(Roles = "Admin")]
        [HttpGet("users")]
        public async Task<IActionResult> GetUsers()
            => await GetUsersWithRoleAsync("User");

        [Authorize(Roles = "Admin")]
        [HttpGet("users/{id}")]
        public async Task<IActionResult> GetUsers(string id)
            => await GetUserInternalAsync(id, "User");

        [Authorize(Roles = "Admin")]
        [HttpPost("moderators")]
        public async Task<IActionResult> PostModerators([FromBody] UserRequest userRequest)
            => await CreateUserInternalAsync(userRequest, "Moderator");

        [Authorize(Roles = "Admin")]
        [HttpGet("moderators")]
        public async Task<IActionResult> GetModerators()
            => await GetUsersWithRoleAsync("Moderator");

        [Authorize(Roles = "Admin")]
        [HttpGet("moderators/{id}")]
        public async Task<IActionResult> GetModerators(string id)
            => await GetUserInternalAsync(id, "Moderator");

        [HttpPost("login")]
        public async Task<IActionResult> PostLogin([FromBody] UserRequest userRequest)
        {
            var result = await _signInManager.PasswordSignInAsync(userRequest?.Email ?? "", userRequest?.Password ?? "", false, false);

            if (result.Succeeded)
            {
                User user = await _userManager.FindByEmailAsync(userRequest?.Email);

                return Ok(new CurrentUserResponse
                {
                    Id = user.Id,
                    Email = user.Email,
                    Roles = await _userManager.GetRolesAsync(user),
                    Token = GenerateJwtToken(userRequest?.Email, user)
                });
            }

            return this.UnauthorizedWithError("Invalid login or password");
        }

        private async Task<IActionResult> GetUserInternalAsync(string id, string userRole)
        {
            var user = await _userManager.FindByIdAsync(id);

            if (user != null && await _userManager.IsInRoleAsync(user, userRole))
            {
                var userResponse = new UserResponse()
                {
                    Id = user.Id,
                    Email = user.Email,
                    Role = userRole
                };

                return Ok(userResponse);
            }

            throw new NotFoundException(userRole, "id", id.ToString());
        }
        private async Task<IActionResult> GetUsersWithRoleAsync(string userRole)
        {
            var users = await _userManager.GetUsersInRoleAsync(userRole);

            var usersResponse = new UsersResponse()
            {
                Count = users.Count(),
                Users = users.Select(u => new UserResponse { Id = u.Id, Email = u.Email, Role = userRole })
            };

            return Ok(usersResponse);
        }
        private async Task<IActionResult> CreateUserInternalAsync(UserRequest userRequest, string userRole)
        {
            var user = new User { Email = userRequest?.Email ?? "", UserName = userRequest?.Email ?? "" };
            var createResult = await _userManager.CreateAsync(user, userRequest?.Password ?? "");

            if (!createResult.Succeeded)
            {
                var errors = new ValidationResult(createResult.Errors.Select(e => e.Description));
                return this.BadRequestWithError(errors);
            }

            await _accountService.CreateUserAsync(new UserDto { Email = user.Email });
            await _userManager.AddToRoleAsync(user, userRole);

            return StatusCode(201);
        }
        private string GenerateJwtToken(string email, User user)
        {
            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, email),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(ClaimTypes.NameIdentifier, user.Id)
            };

            foreach (var role in _userManager.GetRolesAsync(user).Result)
                claims.Add(new Claim(ClaimTypes.Role, role));

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JwtKey"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var expires = DateTime.Now.AddDays(Convert.ToDouble(_configuration["JwtExpireDays"]));

            var token = new JwtSecurityToken(
                _configuration["JwtIssuer"],
                _configuration["JwtIssuer"],
                claims,
                expires: expires,
                signingCredentials: creds
            );

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}
