﻿using System.Threading.Tasks;
using BLL.DTO;
using BLL.Interface;
using FINAL_TASK.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace FINAL_TASK.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AuctionConfigurationController : ControllerBase
    {
        private readonly IAuctionService _auctionService;
        public AuctionConfigurationController(IAuctionService auctionService)
        {
            _auctionService = auctionService;
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var activeAuctionConfiguration = await _auctionService.GetActiveAuctionConfigurationAsync();
            return Ok(activeAuctionConfiguration);
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] AuctionConfigurationDto auctionConfiguration)
        {
            var result = await _auctionService.CreateActiveAuctionConfigurationAsync(auctionConfiguration);

            if (result.IsValid)
                return StatusCode(201);

            return this.BadRequestWithError(result);
        }
    }
}
