﻿using System.Threading.Tasks;
using BLL.DTO;
using BLL.Exceptions;
using BLL.Interface;
using FINAL_TASK.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace FINAL_TASK.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class BrandsController : ControllerBase
    {
        private readonly IProductService _productService;
        private readonly IBrandService _brandService;

        public BrandsController(IProductService productService, IBrandService brandService)
        {
            _productService = productService;
            _brandService = brandService;
        }

        [HttpGet]
        public async Task<IActionResult> GetBrands()
            => Ok(await _brandService.GetBrandsAsync(new BrandSelector(StatusDto.Publicated)));

        [HttpGet("{id}")]
        public async Task<IActionResult> GetBrands(int id)
        {
            var brand = await _brandService.GetBrandAsync(new BrandSelector(id, StatusDto.Publicated));
            if (brand == null)
                throw new NotFoundException("Brand", "id", id.ToString());

            return Ok(brand);
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> PostBrands([FromBody] NewBrandDto newBrand)
        {
            var result = await _brandService.AddBrandAsync(newBrand);

            if (result.IsValid)
                return StatusCode(201);

            return this.BadRequestWithError(result);
        }

        [HttpGet]
        [Route("{brandId}/products")]
        public async Task<IActionResult> GetProducts([FromRoute] int brandId, int limit, int page)
        {
            if (!await _brandService.BrandExistAsync(brandId))
                throw new NotFoundException("Brand", "id", brandId.ToString());

            var productSelector = new ProductSelector(limit, page, ProductStatusDto.Public, null, brandId);
            var products = await _productService.GetProductsAsync(productSelector);
            return Ok(products);
        }
    }
}
