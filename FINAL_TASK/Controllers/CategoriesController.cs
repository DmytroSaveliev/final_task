﻿using System.Threading.Tasks;
using BLL.DTO;
using BLL.Exceptions;
using BLL.Interface;
using FINAL_TASK.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace FINAL_TASK.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CategoriesController : ControllerBase
    {
        private readonly IProductService _productService;
        private readonly ICategoryService _categoryService;

        public CategoriesController(IProductService productService, ICategoryService categoryService)
        {
            _productService = productService;
            _categoryService = categoryService;
        }

        [HttpGet]
        public async Task<IActionResult> GetCategories()
        {
            var categorySelector = new CategorySelector(StatusDto.Publicated);
            var categories = await _categoryService.GetCategoriesFullAsync(categorySelector, StatusDto.Publicated);
            return Ok(categories);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetCategories(int id)
        {
            var category = await _categoryService.GetCategoryAsync(new CategorySelector(id, StatusDto.Publicated));
            if (category == null)
                throw new NotFoundException("Category", "id", id.ToString());

            return Ok(category);
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> PostCategories([FromBody] NewCategoryDto newCategory)
        {
            var result = await _categoryService.AddCategoryAsync(newCategory);

            if (result.IsValid)
                return StatusCode(201);

            return this.BadRequestWithError(result);
        }

        [HttpGet("{categoryId}/subcategories")]
        public async Task<IActionResult> GetSubCategories([FromRoute] int categoryId)
        {
            if (!await _categoryService.CategoryExistAsync(categoryId, StatusDto.Publicated))
                throw new NotFoundException("Category", "id", categoryId.ToString());

            var subCategorySelector = new SubCategorySelector(categoryId, StatusDto.Publicated);
            var subCategories = await _categoryService.GetSubCategoriesAsync(subCategorySelector);
            return Ok(subCategories);
        }

        [HttpGet("{categoryId}/subcategories/{id}")]
        public async Task<IActionResult> GetSubCategories([FromRoute] int categoryId, [FromRoute] int id)
        {
            if (!await _categoryService.CategoryExistAsync(categoryId, StatusDto.Publicated))
                throw new NotFoundException("Category", "id", categoryId.ToString());

            var subCategory = await _categoryService.GetSubCategoryAsync(new SubCategorySelector(id, categoryId, StatusDto.Publicated));
            if (subCategory == null)
                throw new NotFoundException("Subcategory", "id", id.ToString());

            return Ok(subCategory);
        }

        [Authorize]
        [HttpPost("{categoryId}/subcategories")]
        public async Task<IActionResult> PostSubCategories([FromRoute] int categoryId, [FromBody] NewSubCategoryDto newSubCategory)
        {
            if (!await _categoryService.CategoryExistAsync(categoryId, StatusDto.Publicated))
                throw new NotFoundException("Category", "id", categoryId.ToString());

            var result = await _categoryService.AddSubCategoryAsync(categoryId, newSubCategory);

            if (result.IsValid)
                return StatusCode(201);

            return this.BadRequestWithError(result);
        }

        [HttpGet("{categoryId}/subcategories/{subCategoryId}/products")]
        public async Task<IActionResult> GetProducts([FromRoute] int categoryId, [FromRoute] int subCategoryId, int limit, int page)
        {
            if (!await _categoryService.CategoryExistAsync(categoryId, StatusDto.Publicated))
                throw new NotFoundException("Category", "id", categoryId.ToString());

            if (!await _categoryService.SubCategoryExistAsync(categoryId, subCategoryId, StatusDto.Publicated))
                throw new NotFoundException("SubCategory", "id", subCategoryId.ToString());

            var productSelector = new ProductSelector(limit, page, ProductStatusDto.Public, subCategoryId, null);
            var products = await _productService.GetProductsAsync(productSelector);
            return Ok(products);
        }
    }
}
