﻿using System;
using System.Threading.Tasks;
using BLL.DTO;
using BLL.Exceptions;
using BLL.Interface;
using FINAL_TASK.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace FINAL_TASK.Controllers
{
    [ApiController]
    [Authorize(Roles = "Moderator")]
    [Route("api/[controller]")]
    public class ModerationController : ControllerBase 
    {
        private readonly IProductService _productService;
        private readonly ICategoryService _categoryService;
        private readonly IBrandService _brandService;

        public ModerationController(IProductService productService, ICategoryService categoryService, IBrandService brandService)
        {
            _productService = productService;
            _categoryService = categoryService;
            _brandService = brandService;
        }

        [HttpGet("brands")]
        public async Task<IActionResult> GetBrands(StatusDto status)
            => Ok(await _brandService.GetBrandsAsync(new BrandSelector(status)));

        [HttpGet("brands/{id}")]
        public async Task<IActionResult> GetBrands(int id)
        {
            var brand = await _brandService.GetBrandAsync(new BrandSelector(id));
            if (brand == null)
                throw new NotFoundException("Brand", "id", id.ToString());

            return Ok(brand);
        }

        [HttpPatch("brands/{id}")]
        public async Task<IActionResult> PatchBrands([FromRoute] int id, [FromBody] NewNameAndStatusDto newName)
        {
            var result = await _brandService.ModifyBrandAsync(id, newName);

            if (result.IsValid)
                return StatusCode(204);

            return this.BadRequestWithError(result);
        }

        [HttpDelete("brands/{id}")]
        public async Task<IActionResult> DeleteBrands(int id)
        {
            await _brandService.DeleteBrandAsync(id);
            return StatusCode(204);
        }


        [HttpGet("categories")]
        public async Task<IActionResult> GetCategories(StatusDto? status)
        {
            var categorySelector = new CategorySelector(status);
            var categories = await _categoryService.GetCategoriesFullAsync(categorySelector);
            return Ok(categories);
        }

        [HttpGet("categories/{id}")]
        public async Task<IActionResult> GetCategories(int id)
        {
            var category = await _categoryService.GetCategoryAsync(new CategorySelector(id));
            if (category == null)
                throw new NotFoundException("Category", "id", id.ToString());

            return Ok(category);
        }

        [HttpPatch("categories/{id}")]
        public async Task<IActionResult> PatchCategories([FromRoute] int id, [FromBody] NewNameAndStatusDto newName)
        {
            var result = await _categoryService.ModifyCategoryAsync(id, newName);

            if (result.IsValid)
                return StatusCode(204);

            return this.BadRequestWithError(result);
        }

        [HttpDelete("categories/{id}")]
        public async Task<IActionResult> DeleteCategories([FromRoute] int id)
        {
            await _categoryService.DeleteCategoryAsync(id);
            return StatusCode(204);
        }


        [HttpGet("categories/{categoryId}/subcategories")]
        public async Task<IActionResult> GetSubCategories([FromRoute] int categoryId, StatusDto status)
        {
            var subCategorySelector = new SubCategorySelector(categoryId, status);
            var subCategories = await _categoryService.GetSubCategoriesAsync(subCategorySelector);
            return Ok(subCategories);
        }

        [HttpGet("categories/{categoryId}/subcategories/{id}")]
        public async Task<IActionResult> GetSubCategories([FromRoute] int categoryId, [FromRoute] int id)
        {
            if (!await _categoryService.CategoryExistAsync(categoryId))
                throw new NotFoundException("Category", "id", categoryId.ToString());

            var subCategory = await _categoryService.GetSubCategoryAsync(new SubCategorySelector(id, categoryId));
            if (subCategory == null)
                throw new NotFoundException("Subcategory", "id", id.ToString());

            return Ok(subCategory);
        }

        [HttpPatch("categories/{categoryId}/subcategories/{id}")]
        public async Task<IActionResult> PatchSubCategories([FromRoute] int categoryId, [FromRoute] int id, [FromBody] NewNameAndStatusDto newName)
        {
            var result = await _categoryService.ModifySubCategoryAsync(categoryId, id, newName);

            if (result.IsValid)
                return StatusCode(204);

            return this.BadRequestWithError(result);
        }

        [HttpDelete("categories/{categoryId}/subcategories/{id}")]
        public async Task<IActionResult> DeleteSubCategories([FromRoute] int categoryId, [FromRoute] int id)
        {
            await _categoryService.DeleteSubCategoryAsync(categoryId, id);
            return StatusCode(204);
        }


        [HttpGet("products")]
        public async Task<IActionResult> GetProducts(int limit, int page, ProductStatusDto status)
        {
            var productSelector = new ProductSelector(limit, page, status);
            var products = await _productService.GetProductsAsync(productSelector);
            return Ok(products);
        }

        [HttpGet("products/{id}")]
        public async Task<IActionResult> GetProducts(int id)
        {
            var product = await _productService.GetProductAsync(new ProductSelector(id, null, null, null, null), false);
            if (product == null)
                throw new NotFoundException("Product", "id", id.ToString());

            return Ok(product);
        }

        [HttpPatch("products/{id}")]
        public async Task<IActionResult> PatchProducts([FromRoute] int id, [FromBody] UpdatedProductDto updatedProduct)
        {
            var result = await _productService.ModifyProductAsync(id, updatedProduct);

            if (result.IsValid)
                return StatusCode(204);

            return this.BadRequestWithError(result);
        }
    }
}