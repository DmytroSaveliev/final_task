﻿using System.Threading.Tasks;
using BLL.DTO;
using BLL.Exceptions;
using BLL.Interface;
using FINAL_TASK.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace FINAL_TASK.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ProductsController : ControllerBase
    {
        private readonly IProductService _productService;
        private readonly IBetService _betService;

        public ProductsController(IProductService productService, IBetService betService)
        {
            _productService = productService;
            _betService = betService;
        }

        [HttpGet]
        public async Task<IActionResult> GetProducts(int limit, int page, int? subCategoryId)
        {
            var productSelector = new ProductSelector(limit, page, ProductStatusDto.Public, subCategoryId, null);
            var products = await _productService.GetProductsAsync(productSelector);
            return Ok(products);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetProducts(int id)
        {
            bool includeBetConfiguration = User.IsUser();

            var product = await _productService.GetProductAsync(
                new ProductSelector(
                    id, 
                    new ProductStatusDto[] 
                    {
                        ProductStatusDto.Public,
                        ProductStatusDto.Completed,
                        ProductStatusDto.Expired
                    }, 
                    null, 
                    null
                ), 
                includeBetConfiguration
            );
            if (product == null)
                throw new NotFoundException("Product", "id", id.ToString());

            return Ok(product);
        }

        [Authorize(Roles = "User")]
        [HttpPost]
        public async Task<IActionResult> PostProducts([FromBody] NewProductDto newProduct)
        {
            var result = await _productService.CreateProductAsync(User.GetEmail(), newProduct);

            if (result.IsValid)
                return StatusCode(201);

            return this.BadRequestWithError(result);
        }

        [Authorize]
        [HttpGet("{productId}/bets")]
        public async Task<IActionResult> GetBets([FromRoute] int productId)
        {
            var bets = await _betService.GetBetsAsync(productId);
            return Ok(bets);
        }

        [Authorize(Roles = "User")]
        [HttpPost("{productId}/bets")]
        public async Task<IActionResult> PostBets([FromRoute] int productId, [FromBody] NewBetDto newBet)
        {
            var result = await _betService.CreateBetAsync(User.GetEmail(), productId, newBet);

            if (result.IsValid)
                return StatusCode(201);

            return this.BadRequestWithError(result);
        }
    }
}
