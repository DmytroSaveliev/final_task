﻿using System.Linq;
using System.Threading.Tasks;
using BLL.DTO;
using BLL.Interface;
using BLL.Validation;
using FINAL_TASK.Extensions;
using FINAL_TASK.Identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace FINAL_TASK.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class SeedController : ControllerBase
    {
        private readonly ISeedService _initService;
        private readonly UserManager<User> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;

        public SeedController(ISeedService initService, UserManager<User> userManager, RoleManager<IdentityRole> roleManager)
        {
            _initService = initService;
            _userManager = userManager;
            _roleManager = roleManager;
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] SeedDto seed)
        {
            if (_userManager.Users.Count() > 0 || !_initService.CanInit())
                return this.BadRequestWithError(new ValidationResult("Data has been initialized"));

            var user = new User { Email = seed.AdminEmail, UserName = seed.AdminEmail };
            var createResult = await _userManager.CreateAsync(user, seed.AdminPassword);

            if (!createResult.Succeeded)
            {
                var errors = new ValidationResult(createResult.Errors.Select(e => e.Description));
                return this.BadRequestWithError(errors);
            }

            var admin = new UserDto { Email = seed.AdminEmail };

            var validationResult = _initService.Init(admin, seed);
            if (validationResult.IsValid)
            {
                await _roleManager.CreateAsync(new IdentityRole { Name = "Admin" });
                await _roleManager.CreateAsync(new IdentityRole { Name = "User" });
                await _roleManager.CreateAsync(new IdentityRole { Name = "Moderator" });
                await _userManager.AddToRoleAsync(user, "Admin");
                await _userManager.AddToRoleAsync(user, "Moderator");

                return StatusCode(201);
            }

            await _userManager.DeleteAsync(user);

            return this.BadRequestWithError(validationResult);
        }
    }
}
