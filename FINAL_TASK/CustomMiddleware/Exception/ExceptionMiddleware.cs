﻿using BLL.Exceptions;
using FINAL_TASK.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;

namespace FINAL_TASK.CustomMiddleware
{
    public class ExceptionMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly IWebHostEnvironment _env;
        private readonly ILogger _logger;
        static string _errorDelimeter = "\n=========================================================\n";

        public ExceptionMiddleware(RequestDelegate next, IWebHostEnvironment env, ILogger<Program> logger)
        {
            _next = next;
            _logger = logger;
            _env = env;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch(NotFoundException e)
            {
                var notFoundResponse = new NotFoundErrorResponse(context.Request, e);

                context.Response.ContentType = "application/json";
                context.Response.StatusCode = notFoundResponse.status;
                await context.Response.WriteAsync(JsonConvert.SerializeObject(notFoundResponse));
            }
            catch (Exception e) when (!_env.IsDevelopment())
            {
                _logger.LogError("Message: {message}\nStackTrace: {stackTrace}" + _errorDelimeter, e.Message, e.StackTrace);

                var internalResponse = new InternalErrorResponse(context.Request);

                context.Response.ContentType = "application/json";
                context.Response.StatusCode = internalResponse.status;
                await context.Response.WriteAsync(JsonConvert.SerializeObject(internalResponse));
            }
        }

        private class NotFoundErrorResponse : ErrorResponse
        {
            public NotFoundErrorResponse(HttpRequest request, Exception exception)
            {
                title = "Resource not found";
                type = "https://tools.ietf.org/html/rfc7231#section-6.5.4";
                status = 404;
                method = request.Method;
                path = request.Path;
                errors = new string[] { exception.Message };
            }
        }
        private class InternalErrorResponse : ErrorResponse
        {
            public InternalErrorResponse(HttpRequest request)
            {
                title = "Internal server error";
                type = "https://tools.ietf.org/html/rfc7231#section-6.6.1";
                status = 500;
                method = request.Method;
                path = request.Path;
            }
        }
    }
}
