﻿using BLL.Extensions;
using BLL.Implementation;
using BLL.Infrastructure;
using BLL.Interface;
using Microsoft.Extensions.DependencyInjection;

namespace FINAL_TASK.Extensions
{
    public static class ApplicationServiceCollectionExtension
    {
        public static void AddApplicationServices(this IServiceCollection services)
        {
            services.AddTransient<IAuctionService, AuctionService>();
            services.AddTransient<IProductService, ProductService>();
            services.AddTransient<IAccountService, AccountService>();
            services.AddTransient<IBetService, BetService>();
            services.AddTransient<IWorkerService, WorkerService>();
            services.AddTransient<IBrandService, BrandService>();
            services.AddTransient<ICategoryService, CategoryService>();
            services.AddTransient<ISeedService, SeedService>();
            services.AddTransient<IMapper, Mapper>();

            services.AddBusinessServices();
        }
    }
}
