﻿using System.Linq;
using System.Security.Claims;

namespace FINAL_TASK.Extensions
{
    public static class ClaimsPrincipalExtension
    {
        public static string GetEmail(this ClaimsPrincipal user)
        {
            return user.Claims.FirstOrDefault(c => c.Type == "sub")?.Value;
        }

        public static bool IsModerator(this ClaimsPrincipal user)
        {
            return user.IsInRole("Moderator");
        }

        public static bool IsUser(this ClaimsPrincipal user)
        {
            return user.IsInRole("User");
        }
    }
}
