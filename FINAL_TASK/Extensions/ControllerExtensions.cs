﻿using BLL.Validation.Attributes.Interface;
using FINAL_TASK.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace FINAL_TASK.Extensions
{
    public static class ControllerExtensions
    {
        public static UnauthorizedObjectResult UnauthorizedWithError(this ControllerBase controller, string error) =>
            controller.UnauthorizedWithError(new string[] { error });
        public static UnauthorizedObjectResult UnauthorizedWithError(this ControllerBase controller, IEnumerable<string> errors)
        {
            var errorResponse = new ErrorResponse
            {
                type = "https://tools.ietf.org/html/rfc7235#section-3.1",
                status = 401,
                title = "Authorization error occurred",
                errors = errors,
                method = controller.HttpContext.Request.Method,
                path = controller.HttpContext.Request.Path
            };

            return controller.Unauthorized(errorResponse);
        }
            
        public static BadRequestObjectResult BadRequestWithError(this ControllerBase controller, IValidationResultReader validationResult)
        {
            var errorResponse = new ErrorResponse
            {
                type = "https://tools.ietf.org/html/rfc7231#section-6.5.1",
                status = 400,
                title = "Bad request error occurred",
                errors = validationResult.GetErrors(),
                method = controller.HttpContext.Request.Method,
                path = controller.HttpContext.Request.Path
            };

            return controller.BadRequest(errorResponse);
        }
    }
}
