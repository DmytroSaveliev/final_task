﻿using System.Collections.Generic;

namespace FINAL_TASK.Models
{
    public class ErrorResponse
    {
        public string type { get; set; }
        public string title { get; set; }
        public int status { get; set; }
        public string method { get; set; }
        public string path { get; set; }
        public IEnumerable<string> errors { get; set; }
    }
}
