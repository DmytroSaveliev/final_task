﻿using System.Collections.Generic;

namespace FINAL_TASK.Models
{
    public class UsersResponse
    {
        public int Count { get; set; }

        public IEnumerable<UserResponse> Users { get; set; }
    }

    public class UserResponse
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public string Role { get; set; }
    }

    public class CurrentUserResponse
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public IEnumerable<string> Roles { get; set; }
        public string Token { get; set; }
    }
}
