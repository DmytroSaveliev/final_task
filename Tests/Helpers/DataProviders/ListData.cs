﻿using BLL.DTO;
using BLL.Implementation;
using BLL.Infrastructure;
using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Task25_Advanced.Test.Helpers.DataProviders
{
    public class ListData
    {
        public List<Bet> Bet = new List<Bet>();
        public List<Brand> Brand = new List<Brand>();
        public List<Category> Category = new List<Category>();
        public List<Product> Product = new List<Product>();
        public List<ProductDetails> ProductDetails = new List<ProductDetails>();
        public List<SubCategory> SubCategory = new List<SubCategory>();
        public List<User> User = new List<User>();
        public List<AuctionConfiguration> AuctionConfiguration = new List<AuctionConfiguration>();
        public List<BetConfiguration> BetConfiguration = new List<BetConfiguration>();

        public void Init()
        {
            var uof = new UnitOfWorkMock(this);

            var userDto = new UserDto { Email = "test@example.com" };
            var initial = new SeedDto();

            var auctionConfiguration = new AuctionConfigurationDto
            {
                MaxAuctionPeriod = 864000,
                MinAuctionPeriod = 86400,
                MaxBetCapacity = 999,
                MinBetCapacity = 10,
                BetProlongationDuration = 60,
                AuctionProlongationPeriod = 3600
            };
            initial.AuctionConfiguration = auctionConfiguration;
            initial.AdminEmail = "admin@example.com";
            initial.AdminPassword = "Qwerty0@";
            initial.Categories = new Dictionary<string, IList<string>>
            {
                { "Phones", new List<string> { "iPhone 7", "iPhone 8", "iPhone 9", "iPhone 10", "iPhone 11" }  },
                { "Watches", new List<string> { "iWatch 4", "iWatch 5", "iWatch 6" }  },
                { "Computers", new List<string> { "Mac mini", "Mac Pro", "iMac" }  },
                { "Laptops", new List<string> { "MacBook Air", "MacBook Book Pro" }  },
            };
            initial.Brands = new List<string> { "Apple" };

            var initService = new SeedService(uof.Object, new Mapper());

            var result = initService.Init(userDto, initial);
            if (!result.IsValid)
            {
                throw new Exception(result.GetErrors().Aggregate((a, b) => $"{a}; {b}"));
            }
        }
    }
}
