﻿using DAL.Implementation;
using Microsoft.EntityFrameworkCore;

namespace Task25_Advanced.Test.Helpers.DataProviders
{
    public class TestDbContext : AuctionDbContext
    {
        public TestDbContext()
            : base(GetOptions())
        {
            Database.EnsureCreated();
        }

        private static DbContextOptions<AuctionDbContext> GetOptions()
        {
            return new DbContextOptionsBuilder<AuctionDbContext>().UseSqlite("DataSource = test.db").Options;
        }
    }
}
