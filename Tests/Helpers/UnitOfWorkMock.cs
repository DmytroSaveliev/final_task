﻿using DAL.Entities;
using DAL.Interface;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Task25_Advanced.Test.Helpers.DataProviders;

namespace Task25_Advanced.Test.Helpers
{
    public class UnitOfWorkMock
    {
        public ListData Data;
        public Mock<IUnitOfWork> Mock { get; private set; }
        public IUnitOfWork Object { get => Mock.Object; }

        public UnitOfWorkMock(ListData data)
        {
            Data = data;
            InitMock();
        }

        private void InitMock()
        {
            Mock = new Mock<IUnitOfWork>();

            var auctionConfiguration = GetRepositoryMock(Data.AuctionConfiguration);
            var category = GetRepositoryMock(Data.Category);
            var subCategory = GetRepositoryMock(Data.SubCategory);
            var brand = GetRepositoryMock(Data.Brand);
            var product = GetRepositoryMock(Data.Product);
            var user = GetRepositoryMock(Data.User);


            Mock.Setup(u => u.AuctionConfiguration).Returns(auctionConfiguration.Object);
            Mock.Setup(u => u.Category).Returns(category.Object);
            Mock.Setup(u => u.SubCategory).Returns(subCategory.Object);
            Mock.Setup(u => u.Brand).Returns(brand.Object);
            Mock.Setup(u => u.Product).Returns(product.Object);
            Mock.Setup(u => u.User).Returns(user.Object);
        }

        private Mock<IRepository<TEntity>> GetRepositoryMock<TEntity>(List<TEntity> items) where TEntity : BaseEntity
        {
            var repo = new Mock<IRepository<TEntity>>();

            // Task AddItemAsync(T item)
            repo
                .Setup(x => x.AddItemAsync(It.IsAny<TEntity>()))
                .Returns((TEntity item) => { item.Id = items.Select(x => x.Id).DefaultIfEmpty(0).Max() + 1; return Task.Run(() => { items.Add(item); }); });

            // Task<T> GetItemAsync(int id)
            repo.Setup(r => r.GetItemAsync(It.IsAny<int>())).Returns((int i) => { return Task.Run(() => items.Find(a => a.Id == i)); });

            // IEnumerable<T> GetItems()
            repo
                .Setup(r => r.GetItems())
                .Returns(() => items);

            // IEnumerable<T> GetItems<TResult>(Expression<Func<T, TResult>> navigationPropertyPath)
            repo
                .Setup(r => r.GetItems(It.IsAny<Expression<Func<TEntity, It.IsAnyType>>>()))
                .Returns(() => items);
            
            // Task<IEnumerable<T>> GetItemsAsync<TResult>(Expression<Func<T, TResult>> navigationPropertyPath)
            repo
                .Setup(r => r.GetItemsAsync(It.IsAny<Expression<Func<TEntity, It.IsAnyType>>>()))
                .Returns(() => Task.Run(() => items as IEnumerable<TEntity>));

            // Task<IEnumerable<T>> GetItemsAsync(Expression<Func<T, bool>> predicate)
            repo
                .Setup(r => r.GetItemsAsync(It.IsAny<Expression<Func<TEntity, bool>>>()))
                .Returns((Expression<Func<TEntity, bool>> predicate) => Task.Run(() => items.Where(predicate.Compile())));

            // Task<IEnumerable<T>> GetItemsAsync<TResult>(Expression<Func<T, bool>> predicate, Expression<Func<T, TResult>> navigationPropertyPath)
            repo
                .Setup(r => r.GetItemsAsync(It.IsAny<Expression<Func<TEntity, bool>>>(), It.IsAny<Expression<Func<TEntity, It.IsAnyType>>>()))
                .Returns((Expression<Func<TEntity, bool>> predicate, object _) => Task.Run(() => items.Where(predicate.Compile())));

            // Task<IEnumerable<T>> GetItemsAsync<TResult1, TResult2>(Expression<Func<T, bool>> predicate, Expression<Func<T, TResult1>> navigationPropertyPath1, Expression<Func<T, TResult2>> navigationPropertyPath2)
            repo
                .Setup(r => r.GetItemsAsync(It.IsAny<Expression<Func<TEntity, bool>>>(), It.IsAny<Expression<Func<TEntity, It.IsAnyType>>>(), It.IsAny<Expression<Func<TEntity, It.IsAnyType>>>()))
                .Returns((Expression<Func<TEntity, bool>> predicate, object _, object __) => Task.Run(() => items.Where(predicate.Compile())));

            // Task<int> CountAsync(Expression<Func<T, bool>> predicate)
            repo
                .Setup(r => r.CountAsync(It.IsAny<Expression<Func<TEntity, bool>>>()))
                .Returns((Expression<Func<TEntity, bool>> predicate) => Task.Run(() => items.Count(predicate.Compile())));

            // Task<bool> ExistAsync(Expression<Func<T, bool>> predicate)
            repo
                .Setup(r => r.ExistAsync(It.IsAny<Expression<Func<TEntity, bool>>>()))
                .Returns((Expression<Func<TEntity, bool>> predicate) => Task.Run(() => items.Any(predicate.Compile())));

            // Task RemoveItemAsync(T item)
            repo
                .Setup(r => r.RemoveItemAsync(It.IsAny<TEntity>()))
                .Returns((TEntity item) => Task.Run(() => items.Remove(item)));

            // Task UpdateItemAsync(T item)
            repo
                .Setup(r => r.UpdateItemAsync(It.IsAny<TEntity>()))
                .Returns((TEntity item) => Task.Run(() => items[item.Id] = item));

            return repo;
        }
    }
}