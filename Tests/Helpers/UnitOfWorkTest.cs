﻿using DAL.Entities;
using DAL.Implementation;
using DAL.Interface;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using Task25_Advanced.Test.Helpers.DataProviders;

namespace Task25_Advanced.Test.Helpers
{
    public class UnitOfWorkTest : IUnitOfWork
    {
        private readonly DbContext _dbContext;

        public UnitOfWorkTest()
        {
            _dbContext = new TestDbContext();

            AuctionConfiguration = new AuctionRepository<AuctionConfiguration>(_dbContext);
            Category = new AuctionRepository<Category>(_dbContext);
            SubCategory = new AuctionRepository<SubCategory>(_dbContext);
            Brand = new AuctionRepository<Brand>(_dbContext);
            Product = new AuctionRepository<Product>(_dbContext);
            User = new AuctionRepository<User>(_dbContext);
        }

        public void Clear()
        {
            var tables = new string[]
            {
                "AuctionConfiguration",
                "Bet",
                "BetConfiguration",
                "Brand",
                "Product",
                "ProductDetails",
                "SubCategory",
                "Category",
                "User",
            };

            foreach (var table in tables)
            {
                _dbContext.Database.ExecuteSqlRaw($"delete from {table};");
                _dbContext.Database.ExecuteSqlRaw($"UPDATE SQLITE_SEQUENCE SET seq = 0 WHERE name = '{table}';");
            }
        }

        public IRepository<AuctionConfiguration> AuctionConfiguration { get; }

        public IRepository<Category> Category { get; }

        public IRepository<SubCategory> SubCategory { get; }

        public IRepository<Brand> Brand { get; }

        public IRepository<Product> Product { get; }

        public IRepository<User> User { get; }

        public async Task SaveAsync()
        {
            await _dbContext.SaveChangesAsync();
        }
    }
}