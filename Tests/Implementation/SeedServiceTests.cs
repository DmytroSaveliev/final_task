﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using Task25_Advanced.Test.Helpers;
using DAL.Entities;
using System.Threading.Tasks;
using System.Linq;
using BLL.DTO;
using BLL.Infrastructure;
using Task25_Advanced.Test.Helpers.DataProviders;

namespace BLL.Implementation.Tests
{
    [TestFixture()]
    public class SeedServiceTests
    {
        [Test()]
        public void Init_WithEmptyStorageAndValidParameters_AddsNewUserAndGivenData()
        {
            //arrange
            var data = new ListData();
            var uof = new UnitOfWorkMock(data).Object;
            
            var userDto = new UserDto { Email = "admin@example.com" };
            var initial = new SeedDto();

            var auctionConfiguration = new AuctionConfigurationDto
            {
                MaxAuctionPeriod = 864000,
                MinAuctionPeriod = 86400,
                MaxBetCapacity = 999,
                MinBetCapacity = 10,
                BetProlongationDuration = 60,
                AuctionProlongationPeriod = 3600
            };
            initial.AuctionConfiguration = auctionConfiguration;
            initial.AdminEmail = "admin@example.com";
            initial.AdminPassword = "Qwerty0@";
            initial.Categories = new Dictionary<string, IList<string>>
            {
                { "Phones", new List<string> { "iPhone 7", "iPhone 8", "iPhone 9", "iPhone 10", "iPhone 11" }  },
                { "Watches", new List<string> { "iWatch 4", "iWatch 5", "iWatch 6" }  },
                { "Computers", new List<string> { "Mac mini", "Mac Pro", "iMac" }  },
                { "Laptops", new List<string> { "MacBook Air", "MacBook Book Pro" }  },
            };
            initial.Brands = new List<string> { "Apple" };

            var initService = new SeedService(uof, new Mapper());

            //act
            var result = initService.Init(userDto, initial);

            //assert
            Assert.NotNull(data.User.First(u => u.Email == "admin@example.com"));
            Assert.IsTrue(data.AuctionConfiguration.First(u => u.Id == 1).MinAuctionPeriod == 86400);
            Assert.NotNull(data.Category.First(u => u.Name == "Phones").SubCategories.First(x => x.Name == "iPhone 10"));
            Assert.NotNull(data.Category.First(u => u.Name == "Laptops").SubCategories.First(x => x.Name == "MacBook Book Pro"));
        }

        [Test()]
        public void Init_WithValid_Values_HasNoErrors()
        {
            //arrange
            var uof = new UnitOfWorkTest();
            uof.Clear();
            var userDto = new UserDto { Email = "test@example.com" };
            var initial = new SeedDto();

            var auctionConfiguration = new AuctionConfigurationDto
            {
                Id = 1,
                MaxAuctionPeriod = 864000,
                MinAuctionPeriod = 86400,
                MaxBetCapacity = 999,
                MinBetCapacity = 10,
                BetProlongationDuration = 60,
                AuctionProlongationPeriod = 3600
            };
            initial.AuctionConfiguration = auctionConfiguration;
            initial.AdminEmail = "admin@example.com";
            initial.AdminPassword = "Qwerty0@";
            initial.Categories = new Dictionary<string, IList<string>>
            {
                { "Phones", new List<string> { "iPhone 7", "iPhone 8", "iPhone 9", "iPhone 10", "iPhone 11" }  },
                { "Watches", new List<string> { "iWatch 4", "iWatch 5", "iWatch 6" }  },
                { "Computers", new List<string> { "Mac mini", "Mac Pro", "iMac" }  },
                { "Laptops", new List<string> { "MacBook Air", "MacBook Book Pro" }  },
            };
            initial.Brands = new List<string> { "Apple" };

            var initService = new SeedService(uof, new Mapper());
            
            //act
            var result = initService.Init(userDto, initial);

            //assert
            Assert.IsTrue(result.IsValid);
        }

        [Test()]
        public void CanInit_WithNotEmptyStorage_ReturnsFalse()
        {
            //arrange
            var data = new ListData();
            var uof = new UnitOfWorkMock(data);
            data.Category.Add(new Category());
            var expected = false;

            //act
            var initService = new SeedService(uof.Object, new Mapper());
            var actual = initService.CanInit();

            //assert
            Assert.AreEqual(expected, actual);
        }

        [Test()]
        public void CanInit_WithEmptyStorage_ReturnsTrue()
        {
            //arrange
            var data = new ListData();
            var uof = new UnitOfWorkMock(data);
            var expected = true;

            //act
            var initService = new SeedService(uof.Object, new Mapper());
            var actual = initService.CanInit();

            //assert
            Assert.AreEqual(expected, actual);
        }
    }
}